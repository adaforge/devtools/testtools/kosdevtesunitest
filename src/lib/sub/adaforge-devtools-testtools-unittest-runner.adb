--
--  Copyright (c) 2007, 2008 Tero Koskinen <tero.koskinen@iki.fi>
--
--  Permission to use, copy, modify, and distribute this software for any
--  purpose with or without fee is hereby granted, provided that the above
--  copyright notice and this permission notice appear in all copies.
--
--  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
--  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
--  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
--  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
--  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
--  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
--  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
--
--  SPDX-License-Identifier: ISC
--

with Ada.Command_Line;

with AdaForge.DevTools.TestTools.UnitTest_Name_List;
with AdaForge.DevTools.TestTools.UnitTest_Listeners;
with AdaForge.DevTools.TestTools.UnitTest_Listeners.Basic;

package body AdaForge.DevTools.TestTools.UnitTest.Runner is
   use AdaForge.DevTools.TestTools.UnitTest.Results;

   procedure Run_Suite
     (Suite : in out Test'Class; Reporter : Report_Proc)
   is
      use AdaForge.DevTools.TestTools.UnitTest_Listeners.Basic;

      Listener : UnitTest_Listeners.Basic.Basic_Listener;
      Params   : Parameters.Parameter_Info;
      Tests    : UnitTest_Name_List.List;
   begin
      Parameters.Parse_Parameters (Parameters.NORMAL_PARAMETERS, Params);
      Set_Output_Capture (Listener, Parameters.Capture (Params));

      --  Execute only tests which match to the given name.
      --
      --  Single_Test procedure is somewhat misleading since we
      --  can actually execute more than one test if there are multiple
      --  matching names or if a whole test suite or a test case
      --  matches the given name.
      if Parameters.Single_Test (Params) then
         Tests := Parameters.Test_Names (Params);
         Execute
           (T       => Suite, Test_Names => Tests, Listener => Listener,
            Timeout => Parameters.Timeout (Params));
      else
         Execute (Suite, Listener, Parameters.Timeout (Params));
      end if;

      Reporter (Listener.Main_Result, Params);
      if (Error_Count (Listener.Main_Result) > 0)
      or else (Failure_Count (Listener.Main_Result) > 0)
      then
         Ada.Command_Line.Set_Exit_Status (Ada.Command_Line.Failure);
      end if;
   exception
      when Parameters.Invalid_Parameter =>
         Parameters.Usage;
   end Run_Suite;
end AdaForge.DevTools.TestTools.UnitTest.Runner;
