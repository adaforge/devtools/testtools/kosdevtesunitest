--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: ISC
--  SPDX-Creator: Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2007-2022 Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2023 adaforge MARINE s.a.s. (william.franck@adaforge.io)
--  SPDX-FileContributor: 2023 William J. Franck (william.franck@adaforge.io)
--  --------------------------------------------------------------------------------------

with AdaForge.DevTools.TestTools.UnitTest_Long_AStrings;

with Ada.Characters.Conversions;
use Ada.Characters;

with Ada.Strings;
with Ada.Unchecked_Deallocation;
with Ada.Exceptions;

pragma Elaborate_All (Ada.Exceptions);

package body AdaForge.DevTools.TestTools.UnitTest is

   procedure Assert (Condition : Boolean; Message : Wide_Wide_String) is
   begin
      if not Condition then
         Ada.Exceptions.Raise_Exception (Assertion_Error'Identity, Conversions.To_String (Message));
      end if;
   end Assert;

   procedure Assert_Equal
     (Actual : Data_Type; Expected : Data_Type; Message : Wide_Wide_String)
   is
   begin
      Assert
        (Actual = Expected,
         Message & " (Expected: " & Image (Expected) & "; Got: " &
         Image (Actual) & ")");
   end Assert_Equal;

   procedure Fail (Message : Wide_Wide_String) is
   begin
      Ada.Exceptions.Raise_Exception (Assertion_Error'Identity, Conversions.To_String (Message));
   end Fail;

   procedure Skip (Message : Wide_Wide_String) is
   begin
      Ada.Exceptions.Raise_Exception (Test_Skipped_Error'Identity, Conversions.To_String (Message));
   end Skip;

   use AdaForge.DevTools.TestTools.UnitTest_AStrings;

   --  A few local procedures, so we do not need to duplicate code.
   procedure Free_Test is new Ada.Unchecked_Deallocation
     (Object => Test'Class, Name => Test_Class_Access);

   generic
      with procedure Action is <>;
   procedure Execute_Internal
     (Test_Object     : in out Test'Class;
      Listener_Object : in out UnitTest_Listeners.Result_Listener'Class);
   --  Logic for Execute procedures. Action is specified by the caller.

   --  Helper function to reduce some typing.
   function To_Wide_Wide_Bounded (Source : Wide_Wide_String) return UnitTest_AStrings.Bounded_Wide_Wide_String is
   begin
      return To_Bounded_Wide_Wide_String (Source => Source, Drop => Ada.Strings.Right);
   end To_Wide_Wide_Bounded;

   function Name_In_List
     (Name : UnitTest_AStrings.Bounded_Wide_Wide_String; List_Of_Names : UnitTest_Name_List.List)
      return Boolean
   is
      Pos : UnitTest_Name_List.Cursor := UnitTest_Name_List.First (List_Of_Names);
   begin
      loop
         exit when not UnitTest_Name_List.Is_Valid (Pos);

         if UnitTest_Name_List.Data (Pos) = Name then
            return True;
         end if;

         Pos := UnitTest_Name_List.Next (Pos);
      end loop;
      return False;
   end Name_In_List;

   function Name_In_List
     (Name : Wide_Wide_String; List_Of_Names : UnitTest_Name_List.List) return Boolean
   is
   begin
      return Name_In_List (To_Wide_Wide_Bounded (Name), List_Of_Names);
   end Name_In_List;

   ----------- Indefinite_Test_List -------------------

   package body Indefinite_Test_List is
      procedure Remove (Ptr : Node_Access) is
         procedure Free is new Ada.Unchecked_Deallocation
           (Object => Node, Name => Node_Access);
         My_Ptr : Node_Access := Ptr;
      begin
         Ptr.Next := null;
         Free_Test (My_Ptr.Data);
         My_Ptr.Data := null;
         Free (My_Ptr);
      end Remove;

      procedure Append (Target : in out List; Node_Data : Test'Class) is
         New_Node : constant Node_Access :=
           new Node'(Data => new Test'Class'(Node_Data), Next => null);
      begin
         if Target.Last = null then
            Target.Last  := New_Node;
            Target.First := New_Node;
         else
            Target.Last.Next := New_Node;
            Target.Last      := New_Node;
         end if;
      end Append;

      procedure Clear (Target : in out List) is
         Current_Node : Node_Access := Target.First;
         Next_Node    : Node_Access := null;
      begin
         while Current_Node /= null loop
            Next_Node := Current_Node.Next;
            Remove (Current_Node);
            Current_Node := Next_Node;
         end loop;

         Target.First := null;
         Target.Last  := null;
      end Clear;

      procedure For_Each (Target : List) is
         Current_Node : Node_Access := Target.First;
      begin
         while Current_Node /= null loop
            Action (Current_Node.Data.all);
            Current_Node := Current_Node.Next;
         end loop;
      end For_Each;

      overriding
      procedure Initialize (Target : in out List) is
      begin
         Target.Last  := null;
         Target.First := null;
      end Initialize;

      overriding
      procedure Finalize (Target : in out List) is
      begin
         Clear (Target);
      end Finalize;

      overriding
      procedure Adjust (Target : in out List) is
         Target_Last  : Node_Access := null;
         Target_First : Node_Access := null;
         Current      : Node_Access := Target.First;
         New_Node     : Node_Access;
      begin
         while Current /= null loop
            New_Node :=
              new Node'
                (Data => new Test'Class'(Current.Data.all), Next => null);

            if Target_Last = null then
               Target_First := New_Node;
            else
               Target_Last.Next := New_Node;
            end if;
            Target_Last := New_Node;

            Current := Current.Next;
         end loop;
         Target.First := Target_First;
         Target.Last  := Target_Last;
      end Adjust;
   end Indefinite_Test_List;

   ----------- Test type -------------------

   procedure Set_Up (T : in out Test) is
   begin
      null; -- empty by default
   end Set_Up;

   procedure Tear_Down (T : in out Test) is
   begin
      null; -- empty by default
   end Tear_Down;

   procedure Run
     (T : in out Test; Listener : in out UnitTest_Listeners.Result_Listener'Class)
   is
   begin
      Run (T => Test'Class (T), Listener => Listener, Timeout => 0.0);
   end Run;

   procedure Run
     (T        : in out Test; Test_Names : UnitTest_Name_List.List;
      Listener : in out UnitTest_Listeners.Result_Listener'Class)
   is
   begin
      Run
        (T => Test'Class (T), Test_Names => Test_Names, Listener => Listener,
         Timeout => 0.0);
   end Run;

   procedure Execute_Internal
     (Test_Object     : in out Test'Class;
      Listener_Object : in out UnitTest_Listeners.Result_Listener'Class)
   is
      use AdaForge.DevTools.TestTools.UnitTest_Listeners;
   begin
      --  This Start_Test here is called for Test_Suites and Test_Cases.
      --  Info includes only the name of the test suite/case.
      --
      --  There is a separate Start_Test/End_Test pair for test routines
      --  in the Run (T : in out Test_Case; ...) procedure.
      UnitTest_Listeners.Start_Test
        (Listener_Object,
         (Phase     => TEST_BEGIN,
          Test_Name => To_Bounded_Wide_Wide_String (Get_Name (Test_Object)),
          Test_Kind => CONTAINER));

      Action;

      --  Like Start_Test, only for Test_Suites and Test_Cases.
      UnitTest_Listeners.End_Test
        (Listener_Object,
        (Phase      => TEST_END,
          Test_Name => To_Bounded_Wide_Wide_String (Get_Name (Test_Object)),
          Test_Kind => CONTAINER));
   end Execute_Internal;

   function Test_Count (T : Test; Test_Name : Wide_Wide_String) return Test_Count_Type is
      A_List : UnitTest_Name_List.List := UnitTest_Name_List.Empty_List;
   begin
      UnitTest_Name_List.Append (A_List, To_Wide_Wide_Bounded (Test_Name));

      return Test_Count (Test'Class (T), A_List);
   end Test_Count;

   procedure Execute
     (T : in out Test'Class; Listener : in out UnitTest_Listeners.Result_Listener'Class;
      Timeout :        Test_Duration)
   is
      procedure Run_Impl is
      begin
         Run (T, Listener, Timeout);
      end Run_Impl;

      procedure Execute_Impl is new Execute_Internal (Action => Run_Impl);
   begin
      Execute_Impl (Test_Object => T, Listener_Object => Listener);
   end Execute;

   procedure Execute
     (T        : in out Test'Class; Test_Names : UnitTest_Name_List.List;
      Listener : in out UnitTest_Listeners.Result_Listener'Class;
      Timeout  :        Test_Duration)
   is
      procedure Run_Impl is
      begin
         Run
           (T       => T, Test_Names => Test_Names, Listener => Listener,
            Timeout => Timeout);
      end Run_Impl;

      procedure Execute_Impl is new Execute_Internal (Action => Run_Impl);
   begin
      Execute_Impl (Test_Object => T, Listener_Object => Listener);
   end Execute;

   ----------- Test_Case ------------------------------

   --  Wrap an "object" routine inside a Test_Command record
   --  and add it to the test command list.
   --
   --  Name of the test will be silently cut if it does not
   --  fit completely into UnitTest_AStrings.Bounded_Wide_Wide_String.
   procedure Add_Test_Routine
     (T    : in out Test_Case'Class; Routine : Object_Test_Routine_Access;
      Name :        Wide_Wide_String)
   is
      Command : constant Test_Command :=
        (Command_Kind   => OBJECT, Name => To_Wide_Wide_Bounded (Source => Name),
         Object_Routine => Routine);
   begin
      Test_Command_List.Append (T.Routines, Command);
   end Add_Test_Routine;

   --  Wrap a "simple" routine inside a Test_Command record
   --  and add it to the test command list.
   --
   --  Name of the test will be silently cut if it does not
   --  fit completely into UnitTest_AStrings.Bounded_Wide_Wide_String.
   procedure Add_Test_Routine
     (T    : in out Test_Case'Class; Routine : Simple_Test_Routine_Access;
      Name :        Wide_Wide_String)
   is
      Command : constant Test_Command :=
        (Command_Kind   => SIMPLE, Name => To_Wide_Wide_Bounded (Source => Name),
         Simple_Routine => Routine);
   begin
      Test_Command_List.Append (T.Routines, Command);
   end Add_Test_Routine;

   --  The heart of the package.
   --  Run one test routine (well, Command at this point) and
   --  notify listeners about the result.
   procedure Run_Command
     (Command  :        Test_Command; Info : UnitTest_Listeners.Context;
      Timeout  :        Test_Duration;
      Listener : in out UnitTest_Listeners.Result_Listener'Class;
      T        : in out Test_Case'Class)
   is
      use AdaForge.DevTools.TestTools.UnitTest_Listeners;
      use AdaForge.DevTools.TestTools.UnitTest_Long_AStrings;

      type Test_Status is
        (TEST_PASS, TEST_FAIL, TEST_ERROR, TEST_TIMEOUT, TEST_SKIP);

      protected type Test_Results is
         function Get_Status return Test_Status;
         procedure Set_Status (Value : Test_Status);

         function Get_Message return UnitTest_AStrings.Bounded_Wide_Wide_String;
         procedure Set_Message (Value : UnitTest_AStrings.Bounded_Wide_Wide_String);

         function Get_Long_Message return UnitTest_Long_AStrings.Long_String;
         procedure Set_Long_Message (Value : UnitTest_Long_AStrings.Long_String);
      private
         Status       : Test_Status := TEST_ERROR;
         Message      : UnitTest_AStrings.Bounded_Wide_Wide_String;
         Long_Message : UnitTest_Long_AStrings.Long_String;
      end Test_Results;

      protected body Test_Results is
         function Get_Status return Test_Status is
         begin
            return Status;
         end Get_Status;

         procedure Set_Status (Value : Test_Status) is
         begin
            Status := Value;
         end Set_Status;

         function Get_Message return UnitTest_AStrings.Bounded_Wide_Wide_String is
         begin
            return Message;
         end Get_Message;

         procedure Set_Message (Value : UnitTest_AStrings.Bounded_Wide_Wide_String) is
         begin
            Message := Value;
         end Set_Message;

         function Get_Long_Message return UnitTest_Long_AStrings.Long_String is
         begin
            return Long_Message;
         end Get_Long_Message;

         procedure Set_Long_Message (Value : UnitTest_Long_AStrings.Long_String) is
         begin
            Long_Message := Value;
         end Set_Long_Message;
      end Test_Results;

      Result : Test_Results;

      task type Command_Task is
         entry Start_Command;
         entry End_Command;
      end Command_Task;

      procedure Run_A_Command is
         procedure Set_Status
           (S :        Test_Status; Message : Wide_Wide_String; Long_Message : Wide_Wide_String;
            R : in out Test_Results)
         is
         begin
            R.Set_Status (S);
            R.Set_Message (To_Wide_Wide_Bounded (Source => Message));
            R.Set_Long_Message (To_Long_String (Long_Message));
         end Set_Status;
      begin
         begin
            Run (Command, T);
            Result.Set_Status (TEST_PASS);
         exception
            when E : Assertion_Error =>
               Set_Status
                 (S            => TEST_FAIL,
                  Message      => Conversions.To_Wide_Wide_String (Ada.Exceptions.Exception_Message (E)),
                  Long_Message => Conversions.To_Wide_Wide_String (Ada.Exceptions.Exception_Information (E)),
                  R            => Result);
            when E : Test_Skipped_Error =>
               Set_Status
                 (S            => TEST_SKIP,
                  Message      => Conversions.To_Wide_Wide_String (Ada.Exceptions.Exception_Message (E)),
                  Long_Message => Conversions.To_Wide_Wide_String (Ada.Exceptions.Exception_Information (E)),
                  R            => Result);
            when E : others =>
               Set_Status
                 (S            => TEST_ERROR,
                  Message      => Conversions.To_Wide_Wide_String (Ada.Exceptions.Exception_Message (E)),
                  Long_Message => Conversions.To_Wide_Wide_String (Ada.Exceptions.Exception_Information (E)),
                  R            => Result);
         end;
      end Run_A_Command;

      task body Command_Task is
      begin
         accept Start_Command;
         Run_A_Command;
         accept End_Command;
      end Command_Task;

      Status : Test_Status;

   begin
      if Timeout > 0.0 then
         declare
            Command_Runner : Command_Task;
         begin
            Command_Runner.Start_Command;
            select
               Command_Runner.End_Command;
            or
               delay Duration (Timeout);
               abort Command_Runner;
               Result.Set_Status (TEST_TIMEOUT);
            end select;
         end;
      else
         Run_A_Command;
      end if;
      Status := Result.Get_Status;

      case Status is
         when TEST_PASS =>
            UnitTest_Listeners.Add_Pass (Listener, Info);
         when TEST_FAIL =>
            UnitTest_Listeners.Add_Failure
              (Listener,
              (Phase         => TEST_RUN, Test_Name => Info.Test_Name,
                Test_Kind    => CONTAINER, Routine_Name => Info.Routine_Name,
                Message      => Result.Get_Message,
                Long_Message => UnitTest_Long_AStrings.Null_Long_String));
         when TEST_ERROR =>
            UnitTest_Listeners.Add_Error
              (Listener,
              (Phase => UnitTest_Listeners.TEST_RUN, Test_Name => Info.Test_Name,
                Test_Kind    => CONTAINER, Routine_Name => Info.Routine_Name,
                Message      => Result.Get_Message,
                Long_Message => Result.Get_Long_Message));
         when TEST_TIMEOUT =>
            UnitTest_Listeners.Add_Error
              (Listener,
              (Phase => UnitTest_Listeners.TEST_RUN, Test_Name => Info.Test_Name,
                Test_Kind    => CONTAINER, Routine_Name => Info.Routine_Name,
                Message      => To_Bounded_Wide_Wide_String ("TIMEOUT"),
                Long_Message => UnitTest_Long_AStrings.Null_Long_String));
         when TEST_SKIP =>
            UnitTest_Listeners.Add_Skipped
              (Listener,
              (Phase         => TEST_RUN, Test_Name => Info.Test_Name,
                Test_Kind    => CONTAINER, Routine_Name => Info.Routine_Name,
                Message      => Result.Get_Message,
                Long_Message => UnitTest_Long_AStrings.Null_Long_String));
      end case;
   end Run_Command;

   overriding
   function Get_Name (T : Test_Case) return Wide_Wide_String is
   begin
      return To_Wide_Wide_String (T.Name);
   end Get_Name;

   procedure Run_Internal
     (T : in out Test_Case; Listener : in out UnitTest_Listeners.Result_Listener'Class;
      Command :        Test_Command; Test_Name : Wide_Wide_String; Routine_Name : Wide_Wide_String;
      Timeout :        Test_Duration)
   is
      use AdaForge.DevTools.TestTools.UnitTest_Listeners;
   begin
      UnitTest_Listeners.Start_Test
        (Listener,
         (Phase     => UnitTest_Listeners.TEST_BEGIN,
          Test_Name => To_Bounded_Wide_Wide_String (Test_Name), Test_Kind => ROUTINE));
      Run_Command
        (Command => Command,
         Info    =>
           (Phase        => UnitTest_Listeners.TEST_RUN,
            Test_Name => To_Bounded_Wide_Wide_String (Test_Name), Test_Kind => ROUTINE,
            Routine_Name => To_Bounded_Wide_Wide_String (Routine_Name),
            Message      => UnitTest_AStrings.Null_Bounded_Wide_Wide_String,
            Long_Message => UnitTest_Long_AStrings.Null_Long_String),
         Timeout => Timeout, Listener => Listener, T => T);
      UnitTest_Listeners.End_Test
        (Listener,
        (Phase     => UnitTest_Listeners.TEST_END,
         Test_Name => To_Bounded_Wide_Wide_String (Test_Name), Test_Kind => ROUTINE));
   end Run_Internal;

   --  Run procedure for Test_Case.
   --
   --  Loops over the test routine list and executes the routines.
   overriding
   procedure Run
     (T : in out Test_Case; Listener : in out UnitTest_Listeners.Result_Listener'Class;
      Timeout :        Test_Duration)
   is
      procedure Exec (Cmd : in out Test_Command) is
      begin
         Run_Internal
           (T => T, Listener => Listener, Command => Cmd, Timeout => Timeout,
            Test_Name => Get_Name (T), Routine_Name => To_Wide_Wide_String (Cmd.Name));
      end Exec;

      procedure Run_All is new Test_Command_List.For_Each (Action => Exec);
   begin
      Run_All (T.Routines);
   end Run;

   --  Purpose of the procedure is to run all
   --  test routines with name Test_Name.
   overriding
   procedure Run
     (T        : in out Test_Case; Test_Names : UnitTest_Name_List.List;
      Listener : in out UnitTest_Listeners.Result_Listener'Class;
      Timeout  :        Test_Duration)
   is
      procedure Exec (Cmd : in out Test_Command) is
      begin
         if Name_In_List (Cmd.Name, Test_Names) then
            Run_Internal
              (T            => T, Listener => Listener, Command => Cmd,
               Timeout      => Timeout, Test_Name => Get_Name (T),
               Routine_Name => To_Wide_Wide_String (Cmd.Name));
         end if;
      end Exec;

      procedure Run_All is new Test_Command_List.For_Each (Action => Exec);
   begin
      Run_All (T.Routines);
   end Run;

   overriding
   function Test_Count (T : Test_Case) return Test_Count_Type is
   begin
      return Test_Count_Type (Test_Command_List.Length (T.Routines));
   end Test_Count;

   overriding
   function Test_Count
     (T : Test_Case; Test_Names : UnitTest_Name_List.List) return Test_Count_Type
   is
      Counter : Test_Count_Type := 0;

      procedure Increase (Cmd : in out Test_Command) is
      begin
         if Name_In_List (Cmd.Name, Test_Names) then
            Counter := Counter + 1;
         end if;
      end Increase;

      procedure Count_Commands is new Test_Command_List.For_Each
        (Action => Increase);
   begin
      Count_Commands (T.Routines);

      return Counter;
   end Test_Count;

   overriding
   procedure Finalize (T : in out Test_Case) is
   begin
      Test_Command_List.Clear (T.Routines);
   end Finalize;

   procedure Set_Name (T : in out Test_Case; Name : Wide_Wide_String) is
   begin
      T.Name := To_Wide_Wide_Bounded (Source => Name);
   end Set_Name;

   ----------- Test_Suite -----------------------------

   function Create_Suite (Suite_Name : Wide_Wide_String) return Test_Suite_Access is
   begin
      return
        new Test_Suite'
          (Ada.Finalization.Controlled with
           Suite_Name        => To_Wide_Wide_Bounded (Source => Suite_Name),
           Test_Cases        => Test_List.Empty_List,
           Static_Test_Cases => Indefinite_Test_List.Empty_List);
   end Create_Suite;

   function Create_Suite (Suite_Name : Wide_Wide_String) return Test_Suite is
   begin
      return
        (Ada.Finalization.Controlled with
         Suite_Name        => To_Wide_Wide_Bounded (Source => Suite_Name),
         Test_Cases        => Test_List.Empty_List,
         Static_Test_Cases => Indefinite_Test_List.Empty_List);
   end Create_Suite;

   procedure Add_Test (Suite : in out Test_Suite; T : Test_Class_Access) is
   begin
      Test_List.Append (Suite.Test_Cases, T);
   end Add_Test;

   procedure Add_Test (Suite : in out Test_Suite; T : Test_Suite_Access) is
   begin
      Add_Test (Suite, Test_Class_Access (T));
   end Add_Test;

   procedure Add_Static_Test (Suite : in out Test_Suite; T : Test'Class) is
   begin
      Indefinite_Test_List.Append (Suite.Static_Test_Cases, T);
   end Add_Static_Test;

   overriding
   function Get_Name (T : Test_Suite) return Wide_Wide_String is
   begin
      return To_Wide_Wide_String (T.Suite_Name);
   end Get_Name;

   overriding
   procedure Run
     (T : in out Test_Suite; Listener : in out UnitTest_Listeners.Result_Listener'Class;
      Timeout :        Test_Duration)
   is
      --  Some nested procedure exercises here.
      --
      --  Execute_Cases is for normal test list
      --  and Execute_Static_Cases is for indefinite test list.

      --  A helper procedure which runs Execute for the given test.
      procedure Execute_Test (Current : in out Test'Class) is
      begin
         Execute (Current, Listener, Timeout);
      end Execute_Test;

      procedure Execute_Test_Ptr (Current : in out Test_Class_Access) is
      begin
         Execute (Current.all, Listener, Timeout);
      end Execute_Test_Ptr;

      procedure Execute_Static_Cases is new Indefinite_Test_List.For_Each
        (Action => Execute_Test);
      procedure Execute_Cases is new Test_List.For_Each
        (Action => Execute_Test_Ptr);
   begin
      Execute_Cases (T.Test_Cases);
      Execute_Static_Cases (T.Static_Test_Cases);
   end Run;

   overriding
   procedure Run
     (T        : in out Test_Suite; Test_Names : UnitTest_Name_List.List;
      Listener : in out UnitTest_Listeners.Result_Listener'Class;
      Timeout  :        Test_Duration)
   is
      procedure Execute_Test (Current : in out Test'Class) is
      begin
         if Name_In_List (Get_Name (Current), Test_Names) then
            Execute (T => Current, Listener => Listener, Timeout => Timeout);
         else
            Execute
              (T => Current, Test_Names => Test_Names, Listener => Listener,
               Timeout => Timeout);
         end if;
      end Execute_Test;

      procedure Execute_Test_Ptr (Current : in out Test_Class_Access) is
      begin
         Execute_Test (Current.all);
      end Execute_Test_Ptr;

      procedure Execute_Cases is new Test_List.For_Each
        (Action => Execute_Test_Ptr);

      procedure Execute_Static_Cases is new Indefinite_Test_List.For_Each
        (Action => Execute_Test);
   begin
      if Name_In_List (T.Suite_Name, Test_Names) then
         Run (T, Listener, Timeout);
      else
         Execute_Cases (T.Test_Cases);
         Execute_Static_Cases (T.Static_Test_Cases);
      end if;
   end Run;

   overriding
   function Test_Count (T : Test_Suite) return Test_Count_Type is
      Counter : Test_Count_Type := 0;

      procedure Inc_Counter (Test_Obj : in out Test'Class) is
      begin
         Counter := Counter + Test_Count (Test_Obj);
      end Inc_Counter;

      procedure Inc_Counter_Ptr (Ptr : in out Test_Class_Access) is
      begin
         Counter := Counter + Test_Count (Ptr.all);
      end Inc_Counter_Ptr;
   begin
      declare
         use Test_List;
         procedure Count_All is new For_Each (Action => Inc_Counter_Ptr);
      begin
         Count_All (T.Test_Cases);
      end;

      declare
         use Indefinite_Test_List;
         procedure Count_All is new For_Each (Action => Inc_Counter);
      begin
         Count_All (T.Static_Test_Cases);
      end;

      return Counter;
   end Test_Count;

   overriding
   function Test_Count
     (T : Test_Suite; Test_Names : UnitTest_Name_List.List) return Test_Count_Type
   is
      Counter : Test_Count_Type := 0;

      procedure Handle_Test (Test_Object : in out Test'Class) is
      begin
         if Name_In_List (Get_Name (Test_Object), Test_Names) then
            Counter := Counter + Test_Count (Test_Object);
         else
            Counter := Counter + Test_Count (Test_Object, Test_Names);
         end if;
      end Handle_Test;

      procedure Handle_Test_Ptr (Obj : in out Test_Class_Access) is
      begin
         Handle_Test (Obj.all);
      end Handle_Test_Ptr;

      procedure Count_Static is new Indefinite_Test_List.For_Each
        (Action => Handle_Test);
      procedure Count_Tests is new Test_List.For_Each
        (Action => Handle_Test_Ptr);
   begin
      if Name_In_List (T.Suite_Name, Test_Names) then
         return Test_Count (T);
      end if;

      Count_Tests (T.Test_Cases);
      Count_Static (T.Static_Test_Cases);

      return Counter;
   end Test_Count;

   overriding
   procedure Adjust (T : in out Test_Suite) is
      use Test_List;

      New_List : List := Empty_List;

      procedure Create_Copy (Item : in out Test_Class_Access) is
      begin
         Append (New_List, new Test'Class'(Item.all));
      end Create_Copy;

      procedure Copy_All is new For_Each (Action => Create_Copy);
   begin
      Copy_All (T.Test_Cases);

      T.Test_Cases := New_List;
   end Adjust;

   overriding
   procedure Finalize (T : in out Test_Suite) is
      use Test_List;

      procedure Free_Item (Item : in out Test_Class_Access) is
      begin
         Free_Test (Item);
      end Free_Item;

      procedure Free_All is new For_Each (Action => Free_Item);

   begin
      Free_All (T.Test_Cases);
      Clear (T.Test_Cases);
   end Finalize;

   procedure Release_Suite (T : Test_Suite_Access) is
      procedure Free is new Ada.Unchecked_Deallocation
        (Object => Test_Suite, Name => Test_Suite_Access);
      Ptr : Test_Suite_Access := T;
   begin
      Free (Ptr);
   end Release_Suite;

   procedure Run (Command : Test_Command; T : in out Test_Case'Class) is
   begin
      Set_Up (T);
      begin
         case Command.Command_Kind is
            when SIMPLE =>
               Command.Simple_Routine.all;
            when OBJECT =>
               Command.Object_Routine.all (T);
         end case;
      exception
         when others =>
            --  If a routine raises an exception, we end up here.
            --  Tear_Down needs to be called after every test, so
            --  this is a good place to do it.
            --
            --  Tear_Down also might throw an exception, but that should be ok.
            --  It is just then propagated outside instead of the exception
            --  from the test routine.
            Tear_Down (T);
            raise;
      end;

      --  Normal case, no exceptions
      Tear_Down (T);
   end Run;

end AdaForge.DevTools.TestTools.UnitTest;
