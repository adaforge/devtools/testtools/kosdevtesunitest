--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: ISC
--  SPDX-Creator: Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2007-2022 Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2023 adaforge MARINE s.a.s. (william.franck@adaforge.io)
--  SPDX-FileContributor: 2023 William J. Franck (william.franck@adaforge.io)
--  --------------------------------------------------------------------------------------

with Ada.Strings.Fixed;
with Ada.Characters.Conversions;
use Ada.Characters;

package body AdaForge.DevTools.TestTools.UnitTest.Temporary_Output is
   use AdaForge.DevTools.TestTools.UnitTest_AStrings;
   use Ada.Strings.Fixed;
   subtype Temp_Counter_Type is Long_Integer;
   Temp_Counter : Temp_Counter_Type := 0;

   procedure Create_Temp (File : out Temporary_File) is
      Filename : constant Wide_Wide_String :=
        "adaforge-devtools-testtools-unittest_temp_" &
        Conversions.To_Wide_Wide_String (Trim (Long_Integer'Image (Temp_Counter), Ada.Strings.Both));
   begin
      if Temp_Counter < Temp_Counter_Type'Last then
         Temp_Counter := Temp_Counter + 1;
      else
         raise Temporary_File_Error;
      end if;

      File.Name := To_Bounded_Wide_Wide_String (Filename);

      Ada.Text_IO.Create (File.Handle, Ada.Text_IO.Out_File, Conversions.To_String (Filename));
   end Create_Temp;

   function Get_Name (File : Temporary_File) return Wide_Wide_String is
   begin
      return  To_Wide_Wide_String (File.Name);
   end Get_Name;

   procedure Redirect_Output (To_File : in out Temporary_File) is
   begin
      Ada.Text_IO.Flush;
      Ada.Text_IO.Set_Output (To_File.Handle);
   end Redirect_Output;

   procedure Restore_Output is
   begin
      Ada.Text_IO.Flush;
      Ada.Text_IO.Set_Output (Ada.Text_IO.Standard_Output);
   end Restore_Output;

   procedure Remove_Temp (File : in out Temporary_File) is
   begin
      if not Ada.Text_IO.Is_Open (File.Handle) then
         Ada.Text_IO.Open
           (File.Handle, Ada.Text_IO.Out_File, Conversions.To_String (Get_Name (File)));
      end if;
      Ada.Text_IO.Delete (File.Handle);
   end Remove_Temp;

   procedure Close_Temp (File : in out Temporary_File) is
   begin
      Ada.Text_IO.Close (File.Handle);
   end Close_Temp;

end AdaForge.DevTools.TestTools.UnitTest.Temporary_Output;
