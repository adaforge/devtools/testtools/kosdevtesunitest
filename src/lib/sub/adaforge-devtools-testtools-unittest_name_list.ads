--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: ISC
--  SPDX-Creator: Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2007-2022 Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2023 adaforge MARINE s.a.s. (william.franck@adaforge.io)
--  SPDX-FileContributor: 2023 William J. Franck (william.franck@adaforge.io)
--  --------------------------------------------------------------------------------------
--
--  @summary
--
--  @description
--
--  --------------------------------------------------------------------------------------

with AdaForge.DevTools.TestTools.UnitTest_SList;
with AdaForge.DevTools.TestTools.UnitTest_AStrings;

pragma Elaborate_All (AdaForge.DevTools.TestTools.UnitTest_SList);

package AdaForge.DevTools.TestTools.UnitTest_Name_List is new AdaForge.DevTools.TestTools.UnitTest_SList
  (Element_Type => AdaForge.DevTools.TestTools.UnitTest_AStrings.Bounded_Wide_Wide_String);
