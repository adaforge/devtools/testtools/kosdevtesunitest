--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: ISC
--  SPDX-Creator: Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2007-2022 Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2023 adaforge MARINE s.a.s. (william.franck@adaforge.io)
--  SPDX-FileContributor: 2023 William J. Franck (william.franck@adaforge.io)
--  --------------------------------------------------------------------------------------

package body AdaForge.DevTools.TestTools.UnitTest_Listeners is
   procedure Add_Skipped (Listener : in out Result_Listener; Info : Context) is
   begin
      --  By default, we treat skipped tests as Failures.
      --  This is for compatibility with earlier API
      --  which did not know Skipped tests.
      Add_Failure (Result_Listener'Class (Listener), Info);
   end Add_Skipped;
end AdaForge.DevTools.TestTools.UnitTest_Listeners;
