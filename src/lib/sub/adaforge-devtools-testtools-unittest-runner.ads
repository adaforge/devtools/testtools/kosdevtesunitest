--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: ISC
--  SPDX-Creator: Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2007-2022 Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2023 adaforge MARINE s.a.s. (william.franck@adaforge.io)
--  SPDX-FileContributor: 2023 William J. Franck (william.franck@adaforge.io)
--  --------------------------------------------------------------------------------------
--
--  @summary
--
--  @description
--
--  --------------------------------------------------------------------------------------

with AdaForge.DevTools.TestTools.UnitTest.Results;
with AdaForge.DevTools.TestTools.UnitTest.Parameters;

package AdaForge.DevTools.TestTools.UnitTest.Runner is

   type Report_Proc is
     access procedure
       (Test_Results : Results.Result_Collection;
        Args         : Parameters.Parameter_Info);

   procedure Run_Suite
     (Suite : in out Test'Class; Reporter : Report_Proc);
   --  Run the given test (case/suite) and pass the results and
   --  the command line argument info to the reporter procedure.

end AdaForge.DevTools.TestTools.UnitTest.Runner;
