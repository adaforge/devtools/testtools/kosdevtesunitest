--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: ISC
--  SPDX-Creator: Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2007-2022 Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2023 adaforge MARINE s.a.s. (william.franck@adaforge.io)
--  SPDX-FileContributor: 2023 William J. Franck (william.franck@adaforge.io)
--  --------------------------------------------------------------------------------------
--
--  @summary
--
--  @description
--
--  --------------------------------------------------------------------------------------

with Ada.Calendar;
with AdaForge.DevTools.TestTools.UnitTest.Temporary_Output;
with AdaForge.DevTools.TestTools.UnitTest.Results;
use AdaForge.DevTools.TestTools.UnitTest.Results;

use AdaForge.DevTools.TestTools.UnitTest;

pragma Elaborate_All (AdaForge.DevTools.TestTools.UnitTest.Results);
pragma Elaborate_All (AdaForge.DevTools.TestTools.UnitTest.Temporary_Output);

package AdaForge.DevTools.TestTools.UnitTest_Listeners.Basic is
   type Result_Type is
     (NO_RESULT, PASS_RESULT, FAILURE_RESULT, ERROR_RESULT, SKIPPED_RESULT);

   type Basic_Listener is new Result_Listener with record
      Main_Result      : aliased Result_Collection;
      Current_Result   : Result_Collection_Access;
      Last_Test_Result : Result_Type := NO_RESULT;
      Last_Info        : Result_Info := Empty_Result_Info;
      Capture_Output   : Boolean     := False;
      Output_File      : Temporary_Output.Temporary_File;
      Start_Time       : Ada.Calendar.Time;
   end record;

   overriding
   procedure Add_Pass (Listener : in out Basic_Listener; Info : Context);
   --  New implementation for Listeners.Add_Pass

   overriding
   procedure Add_Failure (Listener : in out Basic_Listener; Info : Context);
   --  New implementation for Listeners.Add_Failure

   overriding
   procedure Add_Skipped (Listener : in out Basic_Listener; Info : Context);
   --  New implementation for Listeners.Add_Skipped

   overriding
   procedure Add_Error (Listener : in out Basic_Listener; Info : Context);
   --  New implementation for Listeners.Add_Error

   overriding
   procedure Start_Test (Listener : in out Basic_Listener; Info : Context);
   --  New implementation for Listeners.Start_Test

   overriding
   procedure End_Test (Listener : in out Basic_Listener; Info : Context);
   --  New implementation for Listeners.End_Test

   procedure Set_Output_Capture
     (Listener : in out Basic_Listener; Capture : Boolean);
   --  Enable or disable Ada.Text_IO output capturing

   function Get_Output_Capture (Listener : Basic_Listener) return Boolean;
   --  Capture the Ada.Text_IO output?

private
   procedure Set_Last_Test_Info
     (Listener : in out Basic_Listener; Info : Context; Result : Result_Type);

   procedure Remove_File (Name : Wide_Wide_String);
   procedure Remove_Files (Collection : in out Result_Collection);

   overriding
   procedure Finalize (Listener : in out Basic_Listener);

end AdaForge.DevTools.TestTools.UnitTest_Listeners.Basic;
