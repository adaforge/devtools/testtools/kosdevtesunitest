--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: ISC
--  SPDX-Creator: Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2007-2022 Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2023 adaforge MARINE s.a.s. (william.franck@adaforge.io)
--  SPDX-FileContributor: 2023 William J. Franck (william.franck@adaforge.io)
--  --------------------------------------------------------------------------------------
--
--  @summary
--
--  @description
--
--  --------------------------------------------------------------------------------------

with AdaForge.DevTools.TestTools.UnitTest_Limits;
use AdaForge.DevTools.TestTools.UnitTest_Limits;

with Ada.Strings.Wide_Wide_Bounded;

package AdaForge.DevTools.TestTools.UnitTest_AStrings is new Ada.Strings.Wide_Wide_Bounded.Generic_Bounded_Length
  (Max => Max_String_Len);
