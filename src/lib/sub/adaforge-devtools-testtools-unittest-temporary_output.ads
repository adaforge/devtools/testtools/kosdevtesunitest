--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: ISC
--  SPDX-Creator: Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2007-2022 Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2023 adaforge MARINE s.a.s. (william.franck@adaforge.io)
--  SPDX-FileContributor: 2023 William J. Franck (william.franck@adaforge.io)
--  --------------------------------------------------------------------------------------
--
--  @summary
--
--  @description
--
--  --------------------------------------------------------------------------------------

with AdaForge.DevTools.TestTools.UnitTest_AStrings;
--  use AdaForge.DevTools.TestTools;
with Ada.Text_IO;

package AdaForge.DevTools.TestTools.UnitTest.Temporary_Output is
   Temporary_File_Error : exception;

   type Temporary_File is limited private;

   procedure Create_Temp (File : out Temporary_File);
   --  Create a new temporary file. Exception Temporary_File_Error
   --  is raised if the procedure cannot create a new temp file.

   function Get_Name (File : Temporary_File) return Wide_Wide_String;
   --  Return the name of the file.

   procedure Redirect_Output (To_File : in out Temporary_File);
   --  Redirect the standard output to the file.
   --  To_File must be opened using Create_Temp.

   procedure Restore_Output;
   --  Restore the standard output to its default settings.

   procedure Remove_Temp (File : in out Temporary_File);
   --  Remove the temporary file. File can be either open or closed.

   procedure Close_Temp (File : in out Temporary_File);
   --  Close the temporary file. File needs to be opened first.

private
   type Temporary_File is limited record
      Name   : UnitTest_AStrings.Bounded_Wide_Wide_String;
      Handle : Ada.Text_IO.File_Type;
   end record;

end AdaForge.DevTools.TestTools.UnitTest.Temporary_Output;
