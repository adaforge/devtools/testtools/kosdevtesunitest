--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: ISC
--  SPDX-Creator: Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2007-2022 Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2023 adaforge MARINE s.a.s. (william.franck@adaforge.io)
--  SPDX-FileContributor: 2023 William J. Franck (william.franck@adaforge.io)
--  --------------------------------------------------------------------------------------
--
--  @summary
--
--  @description
--
--  --------------------------------------------------------------------------------------

with Ada.Strings.Wide_Wide_Unbounded;

package AdaForge.DevTools.TestTools.UnitTest_Long_AStrings is

   subtype Long_String is Ada.Strings.Wide_Wide_Unbounded.Unbounded_Wide_Wide_String;

   Null_Long_String : constant Long_String :=
     Ada.Strings.Wide_Wide_Unbounded.Null_Unbounded_Wide_Wide_String;

   function To_Long_String (Str : Wide_Wide_String) return Long_String renames
     Ada.Strings.Wide_Wide_Unbounded.To_Unbounded_Wide_Wide_String;

   function To_Wide_Wide_String (U : Long_String) return Wide_Wide_String renames
     Ada.Strings.Wide_Wide_Unbounded.To_Wide_Wide_String;

   function Length (U : Long_String) return Natural renames
     Ada.Strings.Wide_Wide_Unbounded.Length;

end AdaForge.DevTools.TestTools.UnitTest_Long_AStrings;
