--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: ISC
--  SPDX-Creator: Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2007-2022 Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2023 adaforge MARINE s.a.s. (william.franck@adaforge.io)
--  SPDX-FileContributor: 2023 William J. Franck (william.franck@adaforge.io)
--  --------------------------------------------------------------------------------------

with Ada.Text_IO;
with Ada.Wide_Wide_Text_IO;
with Ada.Strings.Fixed;
with Ada.Characters.Conversions;
use Ada.Characters;

with Ada.Wide_Wide_Characters.Handling;
use Ada.Wide_Wide_Characters.Handling;

with AdaForge.DevTools.TestTools.UnitTest_Name_List;
with AdaForge.DevTools.TestTools.UnitTest.Parameters;
with AdaForge.DevTools.TestTools.UnitTest_AStrings;
with AdaForge.DevTools.TestTools.UnitTest_Long_AStrings;

package body AdaForge.DevTools.TestTools.UnitTest.Tap_Runner is
   use Ada;
   use AdaForge.DevTools.TestTools.UnitTest_AStrings;

   function Count_Image (Count : Test_Count_Type) return String is
      use Ada.Strings;
   begin
      return Fixed.Trim (Test_Count_Type'Image (Count), Both);
   end Count_Image;

   procedure Print_Data (Message : Wide_Wide_String; Prefix : Wide_Wide_String) is
      Start_Of_Line : Boolean := True;
   begin
      for I in Message'Range loop
         if Start_Of_Line then
            Wide_Wide_Text_IO.Put (Prefix);
            Start_Of_Line := False;
         end if;
         if Is_Line_Terminator (Message (I))  then
            Text_IO.New_Line;
            Start_Of_Line := True;
         else
            Wide_Wide_Text_IO.Put (Message (I));
         end if;
      end loop;
      if not Start_Of_Line then
         Text_IO.New_Line;
      end if;
   end Print_Data;

   procedure Run (Suite : in out Test'Class) is
      Listener : Tap_Listener;
      Params   : Parameters.Parameter_Info;
      Tests    : UnitTest_Name_List.List;
   begin
      Text_IO.Put_Line (TAP_Version);

      Parameters.Parse_Parameters (Parameters.TAP_PARAMETERS, Params);

      Listener.Verbose        := Parameters.Verbose (Params);
      Listener.Capture_Output := Parameters.Capture (Params);
      if Parameters.Single_Test (Params) then
         Tests := Parameters.Test_Names (Params);
         Text_IO.Put_Line ("1.." & Count_Image (Test_Count (Suite, Tests)));
         Execute
           (T       => Suite, Test_Names => Tests, Listener => Listener,
            Timeout => Parameters.Timeout (Params));
      else
         Text_IO.Put_Line ("1.." & Count_Image (Test_Count (Suite)));
         Execute (Suite, Listener, Parameters.Timeout (Params));
      end if;
   exception
      when Parameters.Invalid_Parameter =>
         Parameters.Usage (Parameters.TAP_PARAMETERS);
   end Run;

   procedure Print_Info (Info : Context) is
   begin
      if Length (Info.Message) > 0 then
         Print_Data (Message => To_Wide_Wide_String (Info.Message), Prefix => "# ");
      end if;
      if UnitTest_Long_AStrings.Length (Info.Long_Message) > 0 then
         Print_Data
           (Message => UnitTest_Long_AStrings.To_Wide_Wide_String (Info.Long_Message),
            Prefix  => "# ");
      end if;
   end Print_Info;

   procedure Print_Log_File (Filename : Wide_Wide_String; Prefix : Wide_Wide_String) is
      Handle        : Text_IO.File_Type;
      Char          : Character := ' ';
      First         : Boolean   := True;
      Start_Of_Line : Boolean   := True;
   begin
      Text_IO.Open (Handle, Text_IO.In_File, Conversions.To_String (Filename));
      loop
         exit when Text_IO.End_Of_File (Handle);
         Text_IO.Get (Handle, Char);
         if First then
            Wide_Wide_Text_IO.Put_Line (Prefix & "===== Output =======");
            First := False;
         end if;
         if Start_Of_Line then
            Wide_Wide_Text_IO.Put (Prefix);
            Start_Of_Line := False;
         end if;
         Text_IO.Put (Char);
         if Text_IO.End_Of_Line (Handle) then
            Text_IO.New_Line;
            Start_Of_Line := True;
         end if;
      end loop;
      Text_IO.Close (Handle);
      if not First then
         Wide_Wide_Text_IO.Put_Line (Prefix & "====================");
      end if;
   exception
      when Text_IO.Name_Error =>
         --  Missing output file is ok.
         Wide_Wide_Text_IO.Put_Line (Prefix & "no output");
   end Print_Log_File;

   overriding
   procedure Add_Pass (Listener : in out Tap_Listener; Info : Context) is
   begin
      if Listener.Capture_Output then
         Temporary_Output.Restore_Output;
         Temporary_Output.Close_Temp (Listener.Output_File);
      end if;

      Text_IO.Put ("ok ");
      Text_IO.Put (Count_Image (Listener.Current_Test) & " - ");
      Wide_Wide_Text_IO.Put (To_Wide_Wide_String (Info.Test_Name) & ": " & To_Wide_Wide_String (Info.Routine_Name));
      Text_IO.New_Line;
   end Add_Pass;

   procedure Report_Not_Ok (Listener : in out Tap_Listener; Info : Context) is
   begin
      if Listener.Capture_Output then
         Temporary_Output.Restore_Output;
         Temporary_Output.Close_Temp (Listener.Output_File);
      end if;

      Text_IO.Put ("not ok ");
      Text_IO.Put (Count_Image (Listener.Current_Test) & " - ");
      Wide_Wide_Text_IO.Put (To_Wide_Wide_String (Info.Test_Name) & ": " & To_Wide_Wide_String (Info.Routine_Name));
      Text_IO.New_Line;

      if Listener.Verbose then
         Print_Info (Info);
         if Listener.Capture_Output then
            Print_Log_File
              (Filename => Temporary_Output.Get_Name (Listener.Output_File),
               Prefix   => "# ");
         end if;
      end if;
   end Report_Not_Ok;

   overriding
   procedure Add_Failure (Listener : in out Tap_Listener; Info : Context) is
   begin
      Report_Not_Ok (Listener, Info);
   end Add_Failure;

   overriding
   procedure Add_Error (Listener : in out Tap_Listener; Info : Context) is
   begin
      Report_Not_Ok (Listener, Info);
   end Add_Error;

   overriding
   procedure Add_Skipped (Listener : in out Tap_Listener; Info : Context) is
   begin
      if Listener.Capture_Output then
         Temporary_Output.Restore_Output;
         Temporary_Output.Close_Temp (Listener.Output_File);
      end if;

      Text_IO.Put ("ok ");
      Text_IO.Put (Count_Image (Listener.Current_Test) & " - ");
      Wide_Wide_Text_IO.Put (To_Wide_Wide_String (Info.Test_Name) & ": " & To_Wide_Wide_String (Info.Routine_Name));
      Wide_Wide_Text_IO.Put (" # SKIP " & To_Wide_Wide_String (Info.Message));
      Text_IO.New_Line;
   end Add_Skipped;

   overriding
   procedure Start_Test (Listener : in out Tap_Listener; Info : Context) is
   begin
      if Info.Test_Kind = ROUTINE then
         Listener.Current_Test := Listener.Current_Test + 1;
         if Listener.Capture_Output then
            Temporary_Output.Create_Temp (Listener.Output_File);
            Temporary_Output.Redirect_Output (Listener.Output_File);
         end if;
      end if;
   end Start_Test;

   overriding
   procedure End_Test (Listener : in out Tap_Listener; Info : Context) is
      Handle : Text_IO.File_Type;
   begin
      if Listener.Capture_Output then
         Text_IO.Open
           (Handle, Text_IO.Out_File,
            Conversions.To_String (Temporary_Output.Get_Name (Listener.Output_File)));
         Text_IO.Delete (Handle);
      end if;
   exception
      when Text_IO.Name_Error =>
         --  Missing file is safe to ignore, we are going to delete it anyway
         null;
   end End_Test;
end AdaForge.DevTools.TestTools.UnitTest.Tap_Runner;
