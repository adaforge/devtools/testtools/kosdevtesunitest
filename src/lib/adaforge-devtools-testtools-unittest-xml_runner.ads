--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: ISC
--  SPDX-Creator: Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2007-2022 Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2023 adaforge MARINE s.a.s. (william.franck@adaforge.io)
--  SPDX-FileContributor: 2023 William J. Franck (william.franck@adaforge.io)
--  --------------------------------------------------------------------------------------
--
--  @summary
--
--  @description
--
--  --------------------------------------------------------------------------------------

--  with AdaForge.DevTools.TestTools.UnitTest.Framework;
with AdaForge.DevTools.TestTools.UnitTest.Results;
with AdaForge.DevTools.TestTools.UnitTest.Parameters;

package AdaForge.DevTools.TestTools.UnitTest.XML_Runner is

   procedure Run (Suite : in out Test_Suite'Class);
   --  Run the suite and print the results.

   procedure Run (Suite : Test_Suite_Access);
   --  Run the suite and print the results. The routine is
   --  identical to the above routine, but takes an access
   --  parameter to a test suite.

   procedure Report_Results
     (Result : Results.Result_Collection; Dir : Wide_Wide_String; Test_Suffix : Wide_Wide_String);
   --  Report results to the given directory.
private
   procedure Do_Report
     (Test_Results : Results.Result_Collection;
      Args         : Parameters.Parameter_Info);
end AdaForge.DevTools.TestTools.UnitTest.XML_Runner;
