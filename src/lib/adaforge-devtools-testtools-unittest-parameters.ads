--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: ISC
--  SPDX-Creator: Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2007-2022 Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2023 adaforge MARINE s.a.s. (william.franck@adaforge.io)
--  SPDX-FileContributor: 2023 William J. Franck (william.franck@adaforge.io)
--  --------------------------------------------------------------------------------------
--
--  @summary
--
--  @description
--
--  --------------------------------------------------------------------------------------

--  with AdaForge.DevTools.TestTools.UnitTest.Framework;
with AdaForge.DevTools.TestTools.UnitTest_Name_List;

--  A package for handling command line parameters
--
--  Parameters are handled for normal and TAP test runners
--  and the used mode is specified by giving either
--  NORMAL_PARAMETERS or TAP_PARAMETERS parameter to
--  Parse_Parameters procedure.
package AdaForge.DevTools.TestTools.UnitTest.Parameters is

   Invalid_Parameter : exception;

   type Parameter_Info is private;

   type Parameter_Mode is (NORMAL_PARAMETERS, TAP_PARAMETERS);

   procedure Parse_Parameters
     (Mode : Parameter_Mode; Info : out Parameter_Info);
   --  Parse Ada.Command_Line parameters and put the results
   --  to the Info parameter. Raises Invalid_Parameter if
   --  some parameter is invalid.

   procedure Usage (Mode : Parameter_Mode := NORMAL_PARAMETERS);
   --  Print usage.

   function Capture (Info : Parameter_Info) return Boolean;
   --  Capture Ada.Text_IO output?

   function Verbose (Info : Parameter_Info) return Boolean;
   --  Use verbose mode?

   function XML_Results (Info : Parameter_Info) return Boolean;
   --  Output XML?

   function Single_Test (Info : Parameter_Info) return Boolean;
   --  Run a single test (case/suite/routine) only?

   function Test_Name (Info : Parameter_Info) return Wide_Wide_String;
   --  Return the name of the test passed as a parameter.

   function Test_Names (Info : Parameter_Info) return AdaForge.DevTools.TestTools.UnitTest_Name_List.List;
   --  Return all the test names passed as a parameter

   function Result_Dir (Info : Parameter_Info) return Wide_Wide_String;
   --  Return the directory for XML results.

   function Timeout (Info : Parameter_Info) return Test_Duration;
   --  Return the timeout value for a test.

   function Test_Class_Suffix (Info : Parameter_Info) return Wide_Wide_String;

private
   type Parameter_Info is record
      Verbose_Output : Boolean := True;
      Xml_Output     : Boolean := False;
      Capture_Output : Boolean := False;

      Test_Names : UnitTest_Name_List.List;
      --  Names of the wanted tests

      Result_Dir : Natural := 0;
      --  Position of results dir in the argument array

      Test_Suffix : Natural := 0;
      --  Position of test class name suffix in the argument array

      Timeout : Test_Duration := 0.0;
   end record;
end AdaForge.DevTools.TestTools.UnitTest.Parameters;
