--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: ISC
--  SPDX-Creator: Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2007-2022 Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2023 adaforge MARINE s.a.s. (william.franck@adaforge.io)
--  SPDX-FileContributor: 2023 William J. Franck (william.franck@adaforge.io)
--  --------------------------------------------------------------------------------------
--
--  @summary
--
--  @description
--
--  --------------------------------------------------------------------------------------

--  with AdaForge.DevTools.TestTools.UnitTest.Framework;
with AdaForge.DevTools.TestTools.UnitTest_Listeners;
with AdaForge.DevTools.TestTools.UnitTest.Temporary_Output;

package AdaForge.DevTools.TestTools.UnitTest.TAP_Runner is
   --  https://testanything.org
   --  Specifications: https://testanything.org/tap-version-14-specification.html
   --  TAP14 producers must encode TAP data using the UTF-8 encoding

   use AdaForge.DevTools.TestTools.UnitTest_Listeners;

   TAP_Version : constant String := "TAP version 14";

   procedure Run (Suite : in out Test'Class);
   --  Run the suite and print the results.
private
   type TAP_Result_Type is (OK_RESULT, NOT_OK_RESULT);

   type TAP_Listener is new AdaForge.DevTools.TestTools.UnitTest_Listeners.Result_Listener with record
      Result         : TAP_Result_Type       := NOT_OK_RESULT;
      Current_Test   : AdaForge.DevTools.TestTools.UnitTest.Test_Count_Type := 0;
      Verbose        : Boolean               := True;
      Output_File    : Temporary_Output.Temporary_File;
      Capture_Output : Boolean               := False;
   end record;

   overriding
   procedure Add_Pass (Listener : in out TAP_Listener; Info : Context);

   overriding
   procedure Add_Failure (Listener : in out TAP_Listener; Info : Context);

   overriding
   procedure Add_Error (Listener : in out TAP_Listener; Info : Context);

   overriding
   procedure Add_Skipped (Listener : in out TAP_Listener; Info : Context);

   overriding
   procedure Start_Test (Listener : in out TAP_Listener; Info : Context);

   overriding
   procedure End_Test (Listener : in out TAP_Listener; Info : Context);

end AdaForge.DevTools.TestTools.UnitTest.TAP_Runner;
