--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: ISC
--  SPDX-Creator: Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2007-2022 Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2023 adaforge MARINE s.a.s. (william.franck@adaforge.io)
--  SPDX-FileContributor: 2023 William J. Franck (william.franck@adaforge.io)
--  --------------------------------------------------------------------------------------
--
--  @summary
--
--  @description
--
--  --------------------------------------------------------------------------------------
package AdaForge.DevTools.TestTools.UnitTest_Limits is
   pragma Pure;

   Max_String_Len      : constant := 160;
   Max_Long_String_Len : constant := 1_024;
   Count_Max           : constant := (2**31) - 1;
   TimeOut_Guard       : constant := 10_800.0;  --  Three_Hours

end AdaForge.DevTools.TestTools.UnitTest_Limits;
