--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: ISC
--  SPDX-Creator: Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2007-2022 Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2023 adaforge MARINE s.a.s. (william.franck@adaforge.io)
--  SPDX-FileContributor: 2023 William J. Franck (william.franck@adaforge.io)
--  --------------------------------------------------------------------------------------
--
--  @summary
--
--  @description
--
--  --------------------------------------------------------------------------------------

with Ada.Finalization;

with AdaForge.DevTools.TestTools.UnitTest_Limits;
use AdaForge.DevTools.TestTools.UnitTest_Limits;

with AdaForge.DevTools.TestTools.UnitTest_Listeners;
with AdaForge.DevTools.TestTools.UnitTest_SList;
with AdaForge.DevTools.TestTools.UnitTest_AStrings;
with AdaForge.DevTools.TestTools.UnitTest_Name_List;
use AdaForge.DevTools.TestTools;

--  pragma Elaborate_All (AdaForge.DevTools.TestTools.UnitTest);
pragma Elaborate_All (AdaForge.DevTools.TestTools.UnitTest_SList);

package AdaForge.DevTools.TestTools.UnitTest is

   Assertion_Error : exception;
   --  Exception, raised when Assert fails.

   Test_Skipped_Error : exception;
   --  Exception, raised when test is skipped

   subtype Test_Count_Type is Long_Integer range 0 .. Count_Max;
   --  Type for the test count. Long_Integer might be bigger
   --  on some platforms, but the upper limit is something
   --  what most compilers support.
   --
   --  In practice, when adding tests the limit is not explicitly checked.

   procedure Assert (Condition : Boolean; Message : Wide_Wide_String);
   --  If Condition is false, Assert raises Assertion_Error
   --  with given Message.

   generic
      type Data_Type is private;
      with function Image (Item : Data_Type) return Wide_Wide_String is <>;
   procedure Assert_Equal
     (Actual : Data_Type; Expected : Data_Type; Message : Wide_Wide_String);
   --  If Expected /= Actual, Assert raises Assertion_Error
   --  with given Message + represenation of expected and acutal values

   procedure Fail (Message : Wide_Wide_String);
   --  Fail always raises Assertion_Error with given Message.

   procedure Skip (Message : Wide_Wide_String);
   --  Skip always raises Test_Skipped_Error with given Message.

   subtype Test_Duration is Duration range 0.0 .. UnitTest_Limits.TimeOut_Guard;

   type Test is abstract new Ada.Finalization.Controlled with null record;
   --  A type, which provides the base for Test_Case and
   --  Test_Suite types.

   type Test_Class_Access is access all Test'Class;

   procedure Set_Up (T : in out Test);
   --  Set_Up is called before executing the test procedure.
   --
   --  By default, the procedure does nothing, but derived
   --  types can overwrite this method and add their own
   --  customisations.
   --
   --  One should not call this explicitly by herself.
   --  The framework calls it when necessary.

   procedure Tear_Down (T : in out Test);
   --  Tear_Down is called after the test procedure is executed.
   --
   --  By default, the procedure does nothing, but derived
   --  types can overwrite this method and add their own
   --  customisations.
   --
   --  One should not call this explicitly by herself.
   --  The framework calls it when necessary.

   function Get_Name (T : Test) return Wide_Wide_String is abstract;
   --  Return the name of the test.
   --
   --  Types derived from the Test type are required to overwrite
   --  this procedure.

   procedure Run
     (T : in out Test; Listener : in out UnitTest_Listeners.Result_Listener'Class);
   --  Run the test and place the test result to Result.
   --
   --  Calls Run (T, Listener, Timeout) with Timeout value 0.0.

   procedure Run
     (T       : in out Test; Listener : in out UnitTest_Listeners.Result_Listener'Class;
      Timeout :        Test_Duration) is abstract;
   --  Run the test and place the test result to Result.
   --  Timeout specifies the maximum runtime for a single test.
   --
   --  Types derived from the Test type are required to overwrite
   --  this procedure.

   procedure Run
     (T        : in out Test; Test_Names : UnitTest_Name_List.List;
      Listener : in out UnitTest_Listeners.Result_Listener'Class);
   --  Run the test and place the test result to Result.
   --
   --  Calls Run (T, Test_Name, Listener, Timeout) with Timeout value 0.0.

   procedure Run
     (T        : in out Test; Test_Names : UnitTest_Name_List.List;
      Listener : in out UnitTest_Listeners.Result_Listener'Class;
      Timeout  :        Test_Duration) is abstract;
   --  Run the test with given name and place the test result to Result.
   --  Timeout specifies the maximum runtime for a single test.
   --  Notice: If multiple tests have same name this might call all of
   --  them.
   --
   --  Types derived from the Test type are required to overwrite
   --  this procedure.

   function Test_Count (T : Test) return Test_Count_Type is abstract;
   --  Return the amount of tests (test routines) which will be executed when
   --  the Run (T) procedure is called.

   function Test_Count
     (T : Test; Test_Names : UnitTest_Name_List.List)
      return Test_Count_Type is abstract;
   --  Return the amount of tests (test routines) which will be executed when
   --  the Run (T, Test_Names) procedure is called.

   function Test_Count (T : Test; Test_Name : Wide_Wide_String) return Test_Count_Type;
   --  Single name wrapper for Test_Count (T, Test_Names)

   procedure Execute
     (T : in out Test'Class; Listener : in out UnitTest_Listeners.Result_Listener'Class;
      Timeout :        Test_Duration);
   --  Call Test class' Run method and place the test outcome to Result.
   --  The procedure calls Start_Test of every listener before calling
   --  the Run procedure and End_Test after calling the Run procedure.
   --
   --  This procedure is meant to be called from Runner package(s).
   --  There should be no need for other to use this.

   procedure Execute
     (T        : in out Test'Class; Test_Names : UnitTest_Name_List.List;
      Listener : in out UnitTest_Listeners.Result_Listener'Class;
      Timeout  :        Test_Duration);
   --  Same as Execute above, but call the Run procedure which
   --  takes Test_Name parameter.

   type Test_Case is abstract new Test with private;
   --  The base type for other test cases.

   overriding
   function Get_Name (T : Test_Case) return Wide_Wide_String;
   --  Return the name of the test case.

   overriding
   procedure Run
     (T : in out Test_Case; Listener : in out UnitTest_Listeners.Result_Listener'Class;
      Timeout :        Test_Duration);
   --  Run Test_Case's test routines.

   overriding
   procedure Run
     (T        : in out Test_Case; Test_Names : UnitTest_Name_List.List;
      Listener : in out UnitTest_Listeners.Result_Listener'Class;
      Timeout  :        Test_Duration);
   --  Run Test_Case's test routine which matches to the Name.

   overriding
   function Test_Count (T : Test_Case) return Test_Count_Type;
   --  Implementation of Test_Count (T : Test).

   overriding
   function Test_Count
     (T : Test_Case; Test_Names : UnitTest_Name_List.List) return Test_Count_Type;
   --  Implementation of Test_Count (T : Test, Test_Name : UnitTest_Name_List.List).

   overriding
   procedure Finalize (T : in out Test_Case);
   --  Finalize procedure of the Test_Case.

   procedure Set_Name (T : in out Test_Case; Name : Wide_Wide_String);
   --  Set Test_Case's name.
   --
   --  If longer than 160 characters, the name is truncated
   --  to 160 characters.

   type Object_Test_Routine_Access is
     access procedure (T : in out Test_Case'Class);
   --  A pointer to a test routine which takes Test_Case'Class object
   --  as an argument.
   --
   --  For this kind of test routines, the framework will
   --  call Set_Up and Tear_Down routines before and after
   --  test routine execution.

   type Simple_Test_Routine_Access is access procedure;
   --  A pointer to a test routine which does not take arguments.

   procedure Add_Test_Routine
     (T    : in out Test_Case'Class; Routine : Object_Test_Routine_Access;
      Name :        Wide_Wide_String);
   --  Register a test routine to the Test_Case object.
   --
   --  The routine must have signature
   --  "procedure R (T : in out Test_Case'Class)".

   procedure Add_Test_Routine
     (T    : in out Test_Case'Class; Routine : Simple_Test_Routine_Access;
      Name :        Wide_Wide_String);
   --  Register a simple test routine to the Test_Case.
   --
   --  The routine must have signature
   --  "procedure R".

   type Test_Suite is new Test with private;
   --  A collection of Tests.
   --
   --  You can either fill a Test_Suite object with Test_Case objects
   --  or nest multiple Test_Suite objects. You can even mix
   --  Test_Case and Test_Suite objects, if necessary.

   type Test_Suite_Access is access all Test_Suite;

   function Create_Suite (Suite_Name : Wide_Wide_String) return Test_Suite_Access;
   --  Create a new Test_Suite.
   --  Caller must free the returned Test_Suite using Release_Suite.

   function Create_Suite (Suite_Name : Wide_Wide_String) return Test_Suite;
   --  Create a new Test_Suite. The suite and its children are
   --  released automatically.

   procedure Add_Test (Suite : in out Test_Suite; T : Test_Class_Access);
   --  Add a Test to the suite. The suite frees the Test automatically
   --  when it is no longer needed.

   procedure Add_Test (Suite : in out Test_Suite; T : Test_Suite_Access);
   --  Add a Test suite to the suite. The suite frees the Test automatically
   --  when it is no longer needed.
   --
   --  This is a helper function, which internally calls
   --  Add_Test (Suite : in out Test_Suite; T : Test_Class_Access).

   procedure Add_Static_Test (Suite : in out Test_Suite; T : Test'Class);
   --  Add a Test to the suite. This procedure is meant for statically
   --  allocated Test_Case objects.
   --
   --  Please note, that a copy of the Test'Class object is saved to
   --  the suite. Original test object is not modified and changes
   --  made to it after adding the test are not propagated to
   --  the added object.

   overriding
   function Get_Name (T : Test_Suite) return Wide_Wide_String;
   --  Return the name of Test_Suite.

   overriding
   procedure Run
     (T : in out Test_Suite; Listener : in out UnitTest_Listeners.Result_Listener'Class;
      Timeout :        Test_Duration);
   --  Run Test_Suite's Test_Cases.

   overriding
   procedure Run
     (T        : in out Test_Suite; Test_Names : UnitTest_Name_List.List;
      Listener : in out UnitTest_Listeners.Result_Listener'Class;
      Timeout  :        Test_Duration);
   --  Run test suite's child which matches to the given name.

   overriding
   function Test_Count (T : Test_Suite) return Test_Count_Type;
   --  Implementation of Test_Count (T : Test).

   overriding
   function Test_Count
     (T : Test_Suite; Test_Names : UnitTest_Name_List.List) return Test_Count_Type;
   --  Implementation of Test_Count (T : Test, Test_Name : UnitTest_Name_List.List).

   overriding
   procedure Adjust (T : in out Test_Suite);
   --  Adjust procedure of Test_Suite.
   --  Handles the copying of the structure properly

   overriding
   procedure Finalize (T : in out Test_Suite);
   --  Finalize procedure of Test_Suite. Frees all added Tests.

   procedure Release_Suite (T : Test_Suite_Access);
   --  Release the memory of Test_Suite.
   --  All added tests are released automatically.

private
   type Command_Object_Enum is (SIMPLE, OBJECT);

   type Test_Command (Command_Kind : Command_Object_Enum := SIMPLE) is record
      Name : UnitTest_Astrings.Bounded_Wide_Wide_String;
      case Command_Kind is
         when SIMPLE =>
            Simple_Routine : Simple_Test_Routine_Access;
         when OBJECT =>
            Object_Routine : Object_Test_Routine_Access;
      end case;
   end record;
   --  Name attribute tells the name of the test routine.

   procedure Run (Command : Test_Command; T : in out Test_Case'Class);
   --  Run the specified command.
   --  Calls Set_Up and Tear_Down if necessary.

   package Test_Command_List is new AdaForge.DevTools.TestTools.UnitTest_SList (Element_Type => Test_Command);

   type Test_Case is abstract new Test with record
      Routines : Test_Command_List.List  := Test_Command_List.Empty_List;
      Name     : UnitTest_Astrings.Bounded_Wide_Wide_String := UnitTest_Astrings.Null_Bounded_Wide_Wide_String;
   end record;
   --  Our test case type. It holds a list of test routines
   --  (test command objects) and the name of the test case.

   procedure Run_Command
     (Command  :        Test_Command; Info : UnitTest_Listeners.Context;
      Timeout  :        Test_Duration;
      Listener : in out UnitTest_Listeners.Result_Listener'Class;
      T        : in out Test_Case'Class);
   --  Handle dispatching to the right Run (Command : Test_Command)
   --  procedure and record test routine result to the Result object.
   --
   --  Timeout parameter defines the longest time the test is allowed
   --  to run. Value 0.0 means infinite time.

   package Test_List is new AdaForge.DevTools.TestTools.UnitTest_SList (Element_Type => Test_Class_Access);

   package Indefinite_Test_List is
      type List is new Ada.Finalization.Controlled with private;

      Empty_List : constant List;

      procedure Append (Target : in out List; Node_Data : Test'Class);
      --  Append an element at the end of the list.

      procedure Clear (Target : in out List);
      --  Remove all elements from the list.

      generic
         with procedure Action (T : in out Test'Class) is <>;
      procedure For_Each (Target : List);
      --  A generic procedure for walk through every item
      --  in the list and call Action procedure for them.

   private
      type Node;
      type Node_Access is access Node;

      procedure Remove (Ptr : Node_Access);
      --  A procedure to release memory pointed by Ptr.

      type Node is record
         Next : Node_Access       := null;
         Data : Test_Class_Access := null;
      end record;

      type List is new Ada.Finalization.Controlled with record
         First : Node_Access := null;
         Last  : Node_Access := null;
      end record;

      overriding
      procedure Initialize (Target : in out List);
      overriding
      procedure Finalize (Target : in out List);
      overriding
      procedure Adjust (Target : in out List);

      Empty_List : constant List :=
        (Ada.Finalization.Controlled with First => null, Last => null);
   end Indefinite_Test_List;

   type Test_Suite is new Test with record
      Suite_Name        : UnitTest_Astrings.Bounded_Wide_Wide_String   := UnitTest_Astrings.Null_Bounded_Wide_Wide_String;
      Test_Cases        : Test_List.List            := Test_List.Empty_List;
      Static_Test_Cases : Indefinite_Test_List.List :=
        Indefinite_Test_List.Empty_List;
   end record;
   --  A suite type which holds a list of test cases and the name
   --  of the suite.

end AdaForge.DevTools.TestTools.UnitTest;
