--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: ISC
--  SPDX-Creator: Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2007-2022 Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2023 adaforge MARINE s.a.s. (william.franck@adaforge.io)
--  SPDX-FileContributor: 2023 William J. Franck (william.franck@adaforge.io)
--  --------------------------------------------------------------------------------------
--
--  @summary
--
--  @description
--
--  --------------------------------------------------------------------------------------

--  with AdaForge.DevTools.TestTools.UnitTest.Framework;
with AdaForge.DevTools.TestTools.UnitTest.Results;
with AdaForge.DevTools.TestTools.UnitTest.Parameters;

--  pragma Elaborate_All (AdaForge.DevTools.TestTools.UnitTest.Framework);
pragma Elaborate_All (AdaForge.DevTools.TestTools.UnitTest.Results);
pragma Elaborate_All (AdaForge.DevTools.TestTools.UnitTest.Parameters);

package AdaForge.DevTools.TestTools.UnitTest.Text_Runner is
   procedure Run (Suite : in out Test'Class);
   --  Run the suite and print the results.

   procedure Run (Suite : Test_Suite_Access);
   --  Run the suite and print the results.
private
   procedure Do_Report
     (Test_Results : Results.Result_Collection;
      Args         : Parameters.Parameter_Info);
end AdaForge.DevTools.TestTools.UnitTest.Text_Runner;
