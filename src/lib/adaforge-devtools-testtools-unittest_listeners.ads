--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: ISC
--  SPDX-Creator: Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2007-2022 Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2023 adaforge MARINE s.a.s. (william.franck@adaforge.io)
--  SPDX-FileContributor: 2023 William J. Franck (william.franck@adaforge.io)
--  --------------------------------------------------------------------------------------
--
--  @summary
--
--  @description
--
--  --------------------------------------------------------------------------------------

with Ada.Finalization;
with AdaForge.DevTools.TestTools.UnitTest_AStrings;
with AdaForge.DevTools.TestTools.UnitTest_Long_AStrings;
use AdaForge.DevTools.TestTools;

package AdaForge.DevTools.TestTools.UnitTest_Listeners is
   type Test_Phase is (TEST_BEGIN, TEST_RUN, TEST_END);
   --  What is test doing right now?

   type Test_Type is (CONTAINER, ROUTINE);

   type Context (Phase : Test_Phase) is record
      Test_Name : UnitTest_Astrings.Bounded_Wide_Wide_String;
      Test_Kind : Test_Type;
      case Phase is
         when TEST_BEGIN | TEST_END =>
            null;
         when TEST_RUN =>
            Routine_Name : UnitTest_Astrings.Bounded_Wide_Wide_String;
            Message      : UnitTest_Astrings.Bounded_Wide_Wide_String;
            Long_Message : UnitTest_Long_Astrings.Long_String;
      end case;
   end record;

   type Result_Listener is
   abstract new Ada.Finalization.Limited_Controlled with null record;
   --  Result_Listener is a listener for test results.
   --  Whenever a test is run, the framework calls
   --  registered listeners and tells them the result of the test.

   type Result_Listener_Class_Access is access all Result_Listener'Class;

   procedure Add_Pass
     (Listener : in out Result_Listener; Info : Context) is abstract;
   --  Called after test passes.

   procedure Add_Failure
     (Listener : in out Result_Listener; Info : Context) is abstract;
   --  Called after test fails.

   procedure Add_Skipped (Listener : in out Result_Listener; Info : Context);
   --  Called when user wants to skip the test.
   --
   --  By default, skipped tests are treated as failures.
   --  Listeners should reimplement this function if
   --  they want to report skipped tests separately.

   procedure Add_Error
     (Listener : in out Result_Listener; Info : Context) is abstract;
   --  Called after there is an error in the test.

   procedure Start_Test
     (Listener : in out Result_Listener; Info : Context) is abstract;
   --  Called before the test begins. This is called before Add_* procedures.

   procedure End_Test
     (Listener : in out Result_Listener; Info : Context) is abstract;
   --  Called after the test ends. Add_* procedures are called before this.
end AdaForge.DevTools.TestTools.UnitTest_Listeners;
