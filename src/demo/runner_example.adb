--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: ISC
--  SPDX-Creator: Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2007-2022 Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2023 adaforge MARINE s.a.s. (william.franck@adaforge.io)
--  SPDX-FileContributor: 2023 William J. Franck (william.franck@adaforge.io)
--  --------------------------------------------------------------------------------------
--
--  @summary
--
--  @description
--
--  --------------------------------------------------------------------------------------

with AdaForge.DevTools.TestTools.UnitTest.Text_Runner;
--  with AdaForge.DevTools.TestTools.UnitTest.Framework;
with AdaForge.DevTools.TestTools.UnitTest;
use AdaForge.DevTools.TestTools;

with Simple_Tests;

procedure Runner_Example is
   S : UnitTest.Test_Suite := UnitTest.Create_Suite ("All");
begin
   UnitTest.Add_Test (S, new Simple_Tests.Test);
   UnitTest.Text_Runner.Run (S);
end Runner_Example;
