--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: ISC
--  SPDX-Creator: Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2007-2022 Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2023 adaforge MARINE s.a.s. (william.franck@adaforge.io)
--  SPDX-FileContributor: 2023 William J. Franck (william.franck@adaforge.io)
--  --------------------------------------------------------------------------------------

package body Simple1 is

   overriding
   procedure Initialize (T : in out Test_Case) is
   begin
      Set_Name (T, "Simple1");

      UnitTest.Add_Test_Routine
        (T, Test_Simple'Access, "Simple");
   end Initialize;
   
   procedure Test_Simple is
   begin
      null;
   end Test_Simple;

end Simple1;
