--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: ISC
--  SPDX-Creator: Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2007-2022 Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2023 adaforge MARINE s.a.s. (william.franck@adaforge.io)
--  SPDX-FileContributor: 2023 William J. Franck (william.franck@adaforge.io)
--  --------------------------------------------------------------------------------------

with Ada.Text_IO; use Ada.Text_IO;

package body Simple_Tests is

   overriding
   procedure Initialize (T : in out Test) is
   begin
      Set_Name (T, "Simple Tests");

      UnitTest.Add_Test_Routine
        (T, Test_Assertion'Access, "Test Assertion");
      UnitTest.Add_Test_Routine
        (T, Test_With_Object'Access, "Test With Object");
      UnitTest.Add_Test_Routine
        (T, Test_Error'Access, "Test Error (exception)");

   end Initialize;

   overriding
   procedure Set_Up (T : in out Test) is
   begin
      Put_Line ("Simple_Tests.Set_Up");
      T.Value := 1;
   end Set_Up;

   overriding
   procedure Tear_Down (T : in out Test) is
   begin
      Put_Line ("Simple_Tests.Tear_Down");
      T.Value := -1;
   end Tear_Down;

   procedure Test_Assertion is
   begin
      Put_Line ("Test_Assertion");
      UnitTest.Assert (False, "assert(false)");
   end Test_Assertion;

   procedure Test_Error is
   begin
      raise Constraint_Error;
   end Test_Error;

   procedure Hello (T : Test) is
   begin
      UnitTest.Assert (T.Value = 1, "T.Value = 1");
   end Hello;

   procedure Test_With_Object (T : in out UnitTest.Test_Case'Class) is
   begin
      Put_Line ("Test_With_Object");
      Hello (Test (T));
   end Test_With_Object;
end Simple_Tests;
