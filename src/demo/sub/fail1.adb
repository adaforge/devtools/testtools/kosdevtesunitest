--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: ISC
--  SPDX-Creator: Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2007-2022 Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2023 adaforge MARINE s.a.s. (william.franck@adaforge.io)
--  SPDX-FileContributor: 2023 William J. Franck (william.franck@adaforge.io)
--  --------------------------------------------------------------------------------------

package body Fail1 is

   overriding
   procedure Initialize (T : in out Test_Case) is
   begin
      Set_Name (T, "Fail1");

      UnitTest.Add_Test_Routine
        (T, Test_Fail'Access, "Fail");
   end Initialize;

   procedure Test_Fail is
   begin
      UnitTest.Fail ("DOES NOT WORK");
   end Test_Fail;

end Fail1;
