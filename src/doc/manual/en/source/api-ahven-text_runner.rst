.. index:: ! UnitTest.Text_Runner (package)

:mod:`UnitTest.Text_Runner` -- Package
===================================

.. moduleauthor:: Tero Koskinen <tero.koskinen@iki.fi>

------------------------
Procedures and Functions
------------------------


Run
'''

::

   procedure Run (Suite : in out Framework.Test'Class);

Run the suite and print the results.

Run
'''

::

   procedure Run (Suite : Framework.Test_Suite_Access);

Run the suite and print the results.

