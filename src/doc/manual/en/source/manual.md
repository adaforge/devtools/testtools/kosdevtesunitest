# UnitTest User's Guide

© Tero Koskinen <tero.koskinen@iki.fi>

## Overview

### Introduction

UnitTest is a unit test library. It is modeled after
JUnit framework for Java, but some ideas are also
taken from another Ada unit test library, UnitTest.

The purpose of UnitTest is to be a small and portable
unit test library, which works with multiple
different Ada 95 compilers. UnitTest has no
external dependencies and therefore it is easy
to build on various platforms.

UnitTest tries to be compatible with utilities related
to unit testing. For example, it uses same
XML format for test results as Java tools.
This allows easy integration to CruiseControl, Ant,
and other similar programs.

### License

UnitTest is initially distributed under permissive ISC license (shown below).

You are allowed to embed UnitTest into a proprietary commercial application.
Only requirement is to keep the copyright notice and the permission notice
in the source code files. You do not need to distribute UnitTest's source code
if you distribute UnitTest or some parts of UnitTest in the binary form.

## Building and Installing UnitTest

To build and install UnitTest source code, you need an Ada 95 compiler.
At the moment, UnitTest is tested with three different
compiler families: GNAT, Irvine ICCAda, and Janus/Ada.

### GNAT GPL series and FSF GCC

For GNAT there are two sets of project files provided:

* gnat
* gnat_linux

GNAT project files under *gnat* directory are generic and can be used
on any system, but *gnat_linux* depends on GNU Make utility and
expects Unix-like environment (Linux, \*BSD, Cygwin or MinGW on Windows).

#### Using GNAT project files from gnat directory

There are two project files under *gnat* directory:
UnitTest.gpr and UnitTest_tests.gpr.

Project file UnitTest.gpr will build the library itself
and UnitTest_tests.gpr will build the unit tests for the library.

Example on Windows:

```bash
   gnatmake -P gnat\UnitTest
   gnatmake -P gnat\UnitTest_tests
```

Once the source files and tests are compiled, there are two executables
in *gnat* directory: tester(.exe) and tap_tester(.exe).

You can run them and if there are no errors, the library can be expected
to be working correctly.

Project files in *gnat* directory do not provide separate installation
step for the library. When you wish to use the library, you can simply
reference UnitTest.gpr in your project file::

```ada
   --  my_project.gpr
   with "/path/to/UnitTest/gnat/UnitTest.gpr";

   project My_Project is
      -- ...
   end My_Project;
```

### Alternative build system for GNAT on Linux

People using Linux and GNAT, especially Fedora Linux and
FSF GNAT, can use an alternative build system based on
comfignat. This build system integrates better into existing
Ada library infrastructure provided by the used Linux distribution.

To build and install UnitTest using comfignat-based system, run:

```bash
    $ cd gnat_linux
    $ make
    $ sudo make install
```

Note: You need to have *python-sphinx* package installed to generate
the documentation for UnitTest.

If you want to change the installation directory, you can give *make*
command prefix parameter:

```bash
   $ cd gnat_linux
   $ make prefix=$HOME/tmp/UnitTest-install-dir
   $ make install
```


## Using UnitTest

The heart of UnitTest is an abstract type called `Test`.

It presents an entity which can be run by *a test runner*.
Types `Test_Case` and `Test_Suite` are derived from the
`Test` type. The `Test_Case` type is the base type
for unit tests and the `Test_Suite` type is a container,
which can hold other `Test` objects.

### Writing a Test Case

To create a new test case you need to create a new package
and a new type, which is derived from
`UnitTest.Test_Case`.
There are no required functions or procedures to
be implemented, but to make the test case do something
you need to override the `Initialize` procedure
and create at least one procedure which tests something::

```ada
    --  my_tests.ads
    with UnitTest;
    package My_Tests is
       type Test is new UnitTest.Test_Case with null record;
       procedure Initialize (T : in out Test);
    private
       procedure Test_Addition;
    end My_Tests;
```

To add tests to the test case you need to
call procedure `UnitTest.Add_Test_Routine`
during the test case initialization (in other words, in the
`Initialize` procedure).

`testcase_a_body` shows how the
`Test_Addition` is added to the test case.

It also shows how to set a name for the test case with
the `Set_Name` procedure.

### A test case package body

```ada
    --  my_tests.adb
    package body My_Tests is
       procedure Initialize (T : in out Test) is
       begin
          Set_Name (T, "My tests");
          UnitTest.Add_Test_Routine
            (T, Test_Addition'Access, "Addition");
       end Initialize;

       procedure Test_Addition is
       begin
          null;
       end Test_Addition;
    end My_Tests;
```

### Calling Assertion Procedures

To test whether a condition is true or false,
UnitTest offers you three procedures.

The first procedure is `UnitTest.Assert`.
It takes a boolean value and a message `Wide_Wide_String` as its parameters.
If the boolean value is false the `Assert`
raises an `Assertion_Error` exception
with the given string. The exception is caught by the framework
and, when the test results are shown, the error is also shown
with the given message.

Another assertion procedure is a generic
`UnitTest.Assert_Equal` procedure.
It is meant for comparing two objects of same type.
If the objects are not equal
the `Assertion_Error` exception
with the given message string is raised.

The third assertion procedure is simple
`UnitTest.Fail` which always raises
the `Assertion_Error` exception.
It is handy for situations where the execution should not
reach a certain place (see :`fail_example`).

### Fail in action

```ada
    package body My_Tests is
       ...
       procedure Test_My_Proc is
       begin
          begin
             My_Proc (-1); -- should raise Custom_Error
             Fail ("Custom_Error expected");
          exception
             when Custom_Error =>
                null; -- expected
                --  Note: the exception block should not
                --  catch Assertion_Error. Otherwise
                --  the assertion failure will not be noticed.
          end;
       end Test_My_Proc;
    end My_Tests;
```

### Composing Test Hierarchies With Test Suites

The `Test_Suite` type is used to group related tests together.
You can also add other test suites to the suite and create
a hierarchy of tests.

The tests are added to the test suite using either procedure
`Add_Static_Test` or `Add_Test`.

The former procedure is meant for statically created tests and
it places a copy of the given test to the test suite.
The `Add_Test` procedure is used with dynamically created tests
and test objects of type Test_Class_Access.

At the moment, the dynamically added tests are executed first in
the order they have been added (first in, first out - FIFO)
and after them the statically added tests, also in FIFO order.

`suite_example` shows how to put test cases in a test suite.

#### Suite Example

```ada
    package body My_Tests is
       ...
       function Get_Test_Suite return UnitTest.Test_Suite is
          S : Framework.Test_Suite := Framework.Create_Suite ("All");
          Hello_World_Test : Hello_World.Test;
          Listener_Test    : Basic_Listener_Tests.Test;
       begin
          Framework.Add_Static_Test (S, Hello_World_Test);
          Framework.Add_Static_Test (S, Listener_Test);
          return S;
       end Get_Test_Suite;
    end My_Tests;
```

### Running Tests

The tests are run by test runners.  These runners are procedures which take
either test cases or test suites as their parameters.

Currently, there exists three test runners. `UnitTest.Runner` is the basic
runner, which prints the test results as a hierarchy. `UnitTest.XML_Runner`
on the other hand writes the test results to an XML file, which is
understood by continuous integration systems like CruiseControl and Hudson.
The third runner is `UnitTest.Tap_Runner`. It produces the results in
Test-Anything-Protocol (TAP) format.

The recommended way to use these test runners is to call them from
the main program:

```ada
    with UnitTest.Text_Runner;
    with UnitTest;
    with Simple_Tests;
    procedure Tester is
       S : UnitTest.Test_Suite := UnitTest.Create_Suite ("All");
    begin
        UnitTest.Add_Test (S, new Simple_Tests.Test);
        UnitTest.Text_Runner.Run (S);
    end Tester;
```

#### Parameters

`UnitTest.Text_Runner` recognizes following parameters:

tester
* `-d` : **Directory** for test results
* `-x` : output in **XML** format
* `-c` : **Capture** and report test outputs
* `-s` : Specify test name **Suffix** to be used in XML files
* `-t` : specify **Timeout** value for tests (value 0 means infinite timeout)
* `-q` : **Quiet** results
* `-v` : **Verbose** results (default)
* `-i` : ignore remaining parameters - for passing parameters to the test cases
