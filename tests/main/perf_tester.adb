--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: ISC
--  SPDX-Creator: Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2007-2022 Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2023 adaforge MARINE s.a.s. (william.franck@adaforge.io)
--  SPDX-FileContributor: 2023 William J. Franck (william.franck@adaforge.io)
--  --------------------------------------------------------------------------------------

with AdaForge.DevTools.TestTools.UnitTest.Text_Runner;

use AdaForge.DevTools.TestTools;

with Performance_Tests;

procedure Perf_Tester is
   Suite : UnitTest.Test_Suite := Performance_Tests.Get_Test_Suite;
begin
   UnitTest.Text_Runner.Run (Suite);
end Perf_Tester;
