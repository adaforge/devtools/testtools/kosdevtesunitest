--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: ISC
--  SPDX-Creator: Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2007-2022 Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2023 adaforge MARINE s.a.s. (william.franck@adaforge.io)
--  SPDX-FileContributor: 2023 William J. Franck (william.franck@adaforge.io)
--  --------------------------------------------------------------------------------------

with AdaForge.DevTools.TestTools.UnitTest;

with Simple_Listener;

use AdaForge.DevTools.TestTools.UnitTest;

package body Performance_Tests is
   procedure Assert_Eq_Nat is
     new UnitTest.Assert_Equal (Data_Type => Natural,
                             Image     => Natural'Wide_Wide_Image);

   procedure Initialize (T : in out Test) is
   begin
      Set_Name (T, "Run_Tests_X_Times");

      Add_Test_Routine (T, Test_100K_Tests'Access, "Test 100K tests");
      Add_Test_Routine (T, Test_1M_Tests'Access, "Test 1M tests");
      Add_Test_Routine (T, Test_10M_Tests'Access, "Test 10M tests");
      Add_Test_Routine (T, Test_100M_Tests'Access, "Test 100M tests");
   end Initialize;

   procedure Dummy_Test_Routine is
   begin
      null;
   end Dummy_Test_Routine;

   type T is new UnitTest.Test_Case with record
      null;
   end record;

   procedure Run_Tests_X_Times (X : Natural) is
      My_Test : T;
      My_Listener : Simple_Listener.Listener;
   begin
      My_Listener.Passes := 0;

      for I in Natural range 1 .. X loop
         Add_Test_Routine
           (My_Test, Dummy_Test_Routine'Access, "passed test");
      end loop;
      UnitTest.Run (UnitTest.Test_Case (My_Test), My_Listener);
      Assert_Eq_Nat (Actual   => My_Listener.Start_Calls,
                     Expected => X,
                     Message  => "start calls");
      Assert_Eq_Nat (Actual   => My_Listener.End_Calls,
                     Expected => X,
                     Message  => "end calls");
      Assert_Eq_Nat (Actual   => My_Listener.Passes,
                     Expected => X,
                     Message  => "all passed");
   end Run_Tests_X_Times;

   procedure Test_100K_Tests is
   begin
      Run_Tests_X_Times (X => 100_000); --## rule line off STYLE
   end Test_100K_Tests;

   function Get_Test_Suite return UnitTest.Test_Suite is
      S : UnitTest.Test_Suite := UnitTest.Create_Suite ("Performance");

      Perf_Test : Test;
   begin
      UnitTest.Add_Static_Test (S, Perf_Test);
      return S;
   end Get_Test_Suite;

   procedure Test_1M_Tests is
   begin
      Run_Tests_X_Times (X => 1_000_000); --## rule line off STYLE
   end Test_1M_Tests;

   procedure Test_10M_Tests is
   begin
      Run_Tests_X_Times (X => 10_000_000); --## rule line off STYLE
   end Test_10M_Tests;

   procedure Test_100M_Tests is
   begin
      Skip ("Requires too much memory");
      Run_Tests_X_Times (X => 100_000_000); --## rule line off STYLE
   end Test_100M_Tests;
end Performance_Tests;
