--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: ISC
--  SPDX-Creator: Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2007-2022 Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2023 adaforge MARINE s.a.s. (william.franck@adaforge.io)
--  SPDX-FileContributor: 2023 William J. Franck (william.franck@adaforge.io)
--  --------------------------------------------------------------------------------------
--
--  @summary
--
--  @description
--
--  --------------------------------------------------------------------------------------

with AdaForge.DevTools.TestTools.UnitTest;
use AdaForge.DevTools.TestTools;

package Performance_Tests is

   type Test is new UnitTest.Test_Case with null record;

   procedure Initialize (T : in out Test);

   function Get_Test_Suite return UnitTest.Test_Suite;
private
   procedure Test_100K_Tests;
   procedure Test_1M_Tests;
   procedure Test_10M_Tests;
   procedure Test_100M_Tests;
end Performance_Tests;
