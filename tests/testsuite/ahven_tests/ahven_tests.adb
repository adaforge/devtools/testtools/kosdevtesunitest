--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: ISC
--  SPDX-Creator: Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2007-2022 Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2023 adaforge MARINE s.a.s. (william.franck@adaforge.io)
--  SPDX-FileContributor: 2023 William J. Franck (william.franck@adaforge.io)
--  --------------------------------------------------------------------------------------

with Framework_Tests;
with Derived_Tests;
with Results_Tests;
with Basic_Listener_Tests;
with Assertion_Tests;
with Static_Test_Case_Tests;
with SList_Tests;

package body Ahven_Tests is

   function Get_Test_Suite return UnitTest.Test_Suite is
      S : UnitTest.Test_Suite := UnitTest.Create_Suite ("All");

      Assertion_Test : Assertion_Tests.Test;
      Derived_Test   : Derived_Tests.Test;
      Framework_Test : Framework_Tests.Test;
      Listener_Test  : Basic_Listener_Tests.Test;
      Results_Test   : Results_Tests.Test;
      Static_Test    : Static_Test_Case_Tests.Test;
      SList_Test     : SList_Tests.Test;
   begin
      UnitTest.Add_Static_Test (S, Assertion_Test);
      UnitTest.Add_Static_Test (S, Derived_Test);
      UnitTest.Add_Static_Test (S, Framework_Test);
      UnitTest.Add_Static_Test (S, Listener_Test);
      UnitTest.Add_Static_Test (S, Results_Test);
      UnitTest.Add_Static_Test (S, Static_Test);
      UnitTest.Add_Static_Test (S, SList_Test);
      return S;
   end Get_Test_Suite;
end Ahven_Tests;
