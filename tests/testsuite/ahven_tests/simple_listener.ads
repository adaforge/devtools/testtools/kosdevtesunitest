--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: ISC
--  SPDX-Creator: Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2007-2022 Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2023 adaforge MARINE s.a.s. (william.franck@adaforge.io)
--  SPDX-FileContributor: 2023 William J. Franck (william.franck@adaforge.io)
--  --------------------------------------------------------------------------------------
--
--  @summary
--
--  @description
--
--  --------------------------------------------------------------------------------------

with AdaForge.DevTools.TestTools.UnitTest_Listeners;
use AdaForge.DevTools.TestTools;

package Simple_Listener is
   type Listener is new UnitTest_Listeners.Result_Listener with record
      Passes      : Natural := 0;
      Errors      : Natural := 0;
      Failures    : Natural := 0;
      Skips       : Natural := 0;
      Level       : Integer := 0;
      Start_Calls : Natural := 0;
      End_Calls   : Natural := 0;
   end record;

   type Listener_Access is access all Listener;

   overriding
   procedure Add_Pass
     (Object : in out Listener; Info : UnitTest_Listeners.Context);

   overriding
   procedure Add_Failure
     (Object : in out Listener; Info : UnitTest_Listeners.Context);

   overriding
   procedure Add_Skipped
     (Object : in out Listener; Info : UnitTest_Listeners.Context);

   overriding
   procedure Add_Error
     (Object : in out Listener; Info : UnitTest_Listeners.Context);

   overriding
   procedure Start_Test
     (Object : in out Listener; Info : UnitTest_Listeners.Context);

   overriding
   procedure End_Test
     (Object : in out Listener; Info : UnitTest_Listeners.Context);

end Simple_Listener;
