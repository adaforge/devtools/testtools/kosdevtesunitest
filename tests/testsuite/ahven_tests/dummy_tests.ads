--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: ISC
--  SPDX-Creator: Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2007-2022 Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2023 adaforge MARINE s.a.s. (william.franck@adaforge.io)
--  SPDX-FileContributor: 2023 William J. Franck (william.franck@adaforge.io)
--  --------------------------------------------------------------------------------------
--
--  @summary
--
--  @description
--
--  --------------------------------------------------------------------------------------

with AdaForge.DevTools.TestTools.UnitTest;
use AdaForge.DevTools.TestTools;

package Dummy_Tests is
   Dummy_Passes     : constant := 2;
   Dummy_Failures   : constant := 1;
   Dummy_Errors     : constant := 1;
   Dummy_Skips      : constant := 1;
   Dummy_Test_Count : constant :=
     Dummy_Passes + Dummy_Failures + Dummy_Errors + Dummy_Skips;

   type Test_State is (INITIALIZED, UP, DOWN, USED);

   type Test is new UnitTest.Test_Case with record
      State : Test_State;
   end record;

   overriding
   procedure Initialize (T : in out Test);

   overriding
   procedure Adjust (T : in out Test);

   overriding
   procedure Finalize (T : in out Test);

   overriding
   procedure Set_Up (T : in out Test);

   overriding
   procedure Tear_Down (T : in out Test);

   procedure This_Test_Fails;

   procedure This_Test_Passes;

   procedure This_Test_Raises_Error;

   procedure This_Test_Is_Skipped;

   procedure This_Test_Uses_Object
     (T : in out UnitTest.Test_Case'Class);

   procedure This_Test_Takes_12_Seconds;

   procedure This_Test_Has_Infinite_Loop;

   function Get_Instance_Count return Integer;

   procedure Reset_Instance_Count;

   Package_State : Test_State;

   type Test_Simple is new UnitTest.Test_Case with record
      Tear_Down_Count : Integer := -1;
   end record;

   overriding
   procedure Initialize (T : in out Test_Simple);

   overriding
   procedure Set_Up (T : in out Test_Simple);

   overriding
   procedure Tear_Down (T : in out Test_Simple);

   procedure This_Test_Modifies_The_Package_State;

end Dummy_Tests;
