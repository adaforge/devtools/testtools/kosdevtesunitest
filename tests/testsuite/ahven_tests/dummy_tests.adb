--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: ISC
--  SPDX-Creator: Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2007-2022 Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2023 adaforge MARINE s.a.s. (william.franck@adaforge.io)
--  SPDX-FileContributor: 2023 William J. Franck (william.franck@adaforge.io)
--  --------------------------------------------------------------------------------------

--## rule off DIRECTLY_ACCESSED_GLOBALS
package body Dummy_Tests is
   Instance_Count : Integer := 0;

   overriding
   procedure Initialize (T : in out Test) is
      procedure Register
        (T       : in out UnitTest.Test_Case'Class;
         Routine :        UnitTest.Simple_Test_Routine_Access;
         Name    :        Wide_Wide_String) renames
        UnitTest.Add_Test_Routine;
   begin
      Register (T, This_Test_Fails'Access, "Failure");
      Register (T, This_Test_Passes'Access, "Pass");
      Register (T, This_Test_Raises_Error'Access, "Error");
      Register (T, This_Test_Is_Skipped'Access, "Skipped");
      UnitTest.Add_Test_Routine
        (T, This_Test_Uses_Object'Access, "Object usage");
      T.State := INITIALIZED;

      Instance_Count := Instance_Count + 1;
   end Initialize;

   overriding
   procedure Adjust (T : in out Test) is
   begin
      Instance_Count := Instance_Count + 1;
   end Adjust;

   overriding
   procedure Finalize (T : in out Test) is
   begin
      Instance_Count := Instance_Count - 1;
   end Finalize;

   overriding
   procedure Set_Up (T : in out Test) is
   begin
      T.State := UP;
   end Set_Up;

   overriding
   procedure Tear_Down (T : in out Test) is
   begin
      T.State := DOWN;
   end Tear_Down;

   procedure This_Test_Fails is
   begin
      UnitTest.Fail ("Failure");
   end This_Test_Fails;

   procedure This_Test_Passes is
   begin
      UnitTest.Assert (True, "True was not true!");
   end This_Test_Passes;

   procedure This_Test_Raises_Error is
   begin
      raise Constraint_Error;
   end This_Test_Raises_Error;

   procedure This_Test_Is_Skipped is
   begin
      UnitTest.Skip ("skipped");
   end This_Test_Is_Skipped;

   procedure This_Test_Uses_Object (T : in out UnitTest.Test_Case'Class)
   is
   begin
      Test (T).State := USED;
   end This_Test_Uses_Object;

   procedure This_Test_Takes_12_Seconds is
   begin
      delay 12.0;
   end This_Test_Takes_12_Seconds;

   procedure This_Test_Has_Infinite_Loop is
      --## rule off Removable
      Dummy : Integer := 0;
   begin
      loop
         Dummy := 1;
      end loop;
      --## rule on Removable

   end This_Test_Has_Infinite_Loop;

   function Get_Instance_Count return Integer is
   begin
      return Instance_Count;
   end Get_Instance_Count;

   procedure Reset_Instance_Count is
   begin
      Instance_Count := 0;
   end Reset_Instance_Count;

   overriding
   procedure Initialize (T : in out Test_Simple) is
      procedure Register
        (T       : in out UnitTest.Test_Case'Class;
         Routine :        UnitTest.Simple_Test_Routine_Access;
         Name    :        Wide_Wide_String) renames
        UnitTest.Add_Test_Routine;
   begin
      Register
        (T, This_Test_Modifies_The_Package_State'Access,
         "Package state modification");
      Package_State := INITIALIZED;
   end Initialize;

   overriding
   procedure Set_Up (T : in out Test_Simple) is
   begin
      Package_State     := UP;
      T.Tear_Down_Count := 0;
   end Set_Up;

   overriding
   procedure Tear_Down (T : in out Test_Simple) is
   begin
      Package_State     := DOWN;
      T.Tear_Down_Count := T.Tear_Down_Count + 1;
   end Tear_Down;

   procedure This_Test_Modifies_The_Package_State is
   begin
      UnitTest.Assert (Package_State = UP, "Package_State = UP");
      Package_State := USED;
   end This_Test_Modifies_The_Package_State;
end Dummy_Tests;
--## rule on DIRECTLY_ACCESSED_GLOBALS
