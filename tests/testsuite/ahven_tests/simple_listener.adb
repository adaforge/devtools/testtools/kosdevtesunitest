--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: ISC
--  SPDX-Creator: Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2007-2022 Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2023 adaforge MARINE s.a.s. (william.franck@adaforge.io)
--  SPDX-FileContributor: 2023 William J. Franck (william.franck@adaforge.io)
--  --------------------------------------------------------------------------------------

package body Simple_Listener is

   overriding
   procedure Add_Pass
     (Object : in out Listener; Info : UnitTest_Listeners.Context)
   is
   begin
      Object.Passes := Object.Passes + 1;
   end Add_Pass;

   overriding
   procedure Add_Failure
     (Object : in out Listener; Info : UnitTest_Listeners.Context)
   is
   begin
      Object.Failures := Object.Failures + 1;
   end Add_Failure;

   overriding
   procedure Add_Skipped
     (Object : in out Listener; Info : UnitTest_Listeners.Context)
   is
   begin
      Object.Skips := Object.Skips + 1;
   end Add_Skipped;

   overriding
   procedure Add_Error
     (Object : in out Listener; Info : UnitTest_Listeners.Context)
   is
   begin
      Object.Errors := Object.Errors + 1;
   end Add_Error;

   overriding
   procedure Start_Test
     (Object : in out Listener; Info : UnitTest_Listeners.Context)
   is
   begin
      Object.Level       := Object.Level + 1;
      Object.Start_Calls := Object.Start_Calls + 1;
   end Start_Test;

   overriding
   procedure End_Test
     (Object : in out Listener; Info : UnitTest_Listeners.Context)
   is
   begin
      Object.Level     := Object.Level - 1;
      Object.End_Calls := Object.End_Calls + 1;
   end End_Test;

end Simple_Listener;
