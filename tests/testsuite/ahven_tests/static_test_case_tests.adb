--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: ISC
--  SPDX-Creator: Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2007-2022 Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2023 adaforge MARINE s.a.s. (william.franck@adaforge.io)
--  SPDX-FileContributor: 2023 William J. Franck (william.franck@adaforge.io)
--  --------------------------------------------------------------------------------------

with AdaForge.DevTools.TestTools.UnitTest;

with Simple_Listener;
with Dummy_Tests;

package body Static_Test_Case_Tests is
   use AdaForge.DevTools.TestTools.UnitTest;

   --## rule off DIRECTLY_ACCESSED_GLOBALS
   My_Test : Dummy_Tests.Test;
   Child   : Test_Suite := Create_Suite ("Child suite");

   procedure Assert_Eq_Nat is new UnitTest.Assert_Equal
     (Data_Type => Natural, Image => Natural'Wide_Wide_Image);

   overriding
   procedure Initialize (T : in out Test) is
   begin
      Set_Name (T, "UnitTest.Static");

      UnitTest.Add_Test_Routine
        (T, Test_Test_Suite_Run'Access, "Test_Suite: Run (Static)");
      UnitTest.Add_Test_Routine
        (T, Test_Test_Suite_Inside_Suite'Access,
         "Test_Suite: Suite inside another (Static)");
   end Initialize;

   procedure Test_Test_Suite_Run is
      use Dummy_Tests;

      My_Listener : Simple_Listener.Listener;
      My_Suite    : Test_Suite := Create_Suite ("My suite");
   begin
      Add_Static_Test (My_Suite, My_Test);

      Run (My_Suite, My_Listener);

      Assert_Eq_Nat (My_Listener.Passes, Dummy_Passes, "passes");

      Assert_Eq_Nat (My_Listener.Errors, Dummy_Errors, "errors");
      Assert_Eq_Nat (My_Listener.Failures, Dummy_Failures, "failures");
      Assert (My_Listener.Level = 0, "Start_Test /= End_Test");
      Assert_Eq_Nat
        (My_Listener.Start_Calls, (Dummy_Test_Count + 1), "Start_Test calls");
   end Test_Test_Suite_Run;

   procedure Test_Test_Suite_Inside_Suite is
      use Dummy_Tests;

      My_Listener : Simple_Listener.Listener;
      Parent      : Test_Suite := Create_Suite ("Parent suite");
   begin
      UnitTest.Add_Static_Test (Child, My_Test);

      UnitTest.Add_Static_Test (Parent, Child);

      UnitTest.Run (Parent, My_Listener);

      Assert_Eq_Nat (My_Listener.Passes, Dummy_Passes, "passes");
      Assert_Eq_Nat (My_Listener.Errors, Dummy_Errors, "errors");
      Assert_Eq_Nat (My_Listener.Failures, Dummy_Failures, "failures");
      Assert (My_Listener.Level = 0, "Start_Test /= End_Test");
      Assert_Eq_Nat
        (My_Listener.Start_Calls, (Dummy_Test_Count + 2), "Start_Test calls");
   end Test_Test_Suite_Inside_Suite;
end Static_Test_Case_Tests;
