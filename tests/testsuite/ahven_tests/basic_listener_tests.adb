--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: ISC
--  SPDX-Creator: Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2007-2022 Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2023 adaforge MARINE s.a.s. (william.franck@adaforge.io)
--  SPDX-FileContributor: 2023 William J. Franck (william.franck@adaforge.io)
--  --------------------------------------------------------------------------------------

with AdaForge.DevTools.TestTools.UnitTest;
with AdaForge.DevTools.TestTools.UnitTest_Listeners.Basic;
with AdaForge.DevTools.TestTools.UnitTest.Results;
with AdaForge.DevTools.TestTools.UnitTest_AStrings;
with AdaForge.DevTools.TestTools.UnitTest_Long_AStrings;
use AdaForge.DevTools.TestTools.UnitTest;
use AdaForge.DevTools.TestTools.UnitTest.Results;

package body Basic_Listener_Tests is
   procedure Assert_Eq is new UnitTest.Assert_Equal
     (Data_Type => Test_Count_Type, Image => Test_Count_Type'Wide_Wide_Image);

   overriding
   procedure Initialize (T : in out Test) is
   begin
      Set_Name (T, "UnitTest_Listeners.Basic");
      UnitTest.Add_Test_Routine
        (T, Test_Single_Pass'Access, "Test Single Pass");
      UnitTest.Add_Test_Routine
        (T, Test_Error_Inside_Suite'Access, "Test Error Inside Suite");
   end Initialize;

   procedure Test_Single_Pass is
      use AdaForge.DevTools.TestTools.UnitTest_Listeners;
      use AdaForge.DevTools.TestTools.UnitTest_AStrings;

      Listener : Basic.Basic_Listener;
   begin
      UnitTest_Listeners.Basic.Start_Test
        (Listener,
         (Phase     => TEST_BEGIN, Test_Name => To_Bounded_Wide_Wide_String ("testname"),
          Test_Kind => ROUTINE));
      UnitTest_Listeners.Basic.Add_Pass
        (Listener,
        (Phase => TEST_RUN, Test_Name => To_Bounded_Wide_Wide_String ("testname"),
          Test_Kind => ROUTINE, Routine_Name => To_Bounded_Wide_Wide_String ("routine"),
          Message      => To_Bounded_Wide_Wide_String ("message"),
          Long_Message => UnitTest_Long_AStrings.To_Long_String ("long_message")));
      UnitTest_Listeners.Basic.End_Test
        (Listener,
        (Phase      => TEST_END, Test_Name => To_Bounded_Wide_Wide_String ("testname"),
          Test_Kind => ROUTINE));

      Assert_Eq (Test_Count (Listener.Main_Result), 1, "Test Count");
   end Test_Single_Pass;

   procedure Test_Error_Inside_Suite is
      use AdaForge.DevTools.TestTools.UnitTest_Listeners;
      use AdaForge.DevTools.TestTools.UnitTest_AStrings;

      Listener : Basic.Basic_Listener;
   begin
      UnitTest_Listeners.Basic.Start_Test
        (Listener,
         (Phase     => TEST_BEGIN, Test_Name => To_Bounded_Wide_Wide_String ("suite"),
          Test_Kind => CONTAINER));

      UnitTest_Listeners.Basic.Start_Test
        (Listener,
        (Phase      => TEST_BEGIN, Test_Name => To_Bounded_Wide_Wide_String ("testname"),
          Test_Kind => ROUTINE));

      UnitTest_Listeners.Basic.Add_Error
        (Listener,
        (Phase => TEST_RUN, Test_Name => To_Bounded_Wide_Wide_String ("testname"),
          Test_Kind => ROUTINE, Routine_Name => To_Bounded_Wide_Wide_String ("routine"),
          Message      => To_Bounded_Wide_Wide_String ("message"),
          Long_Message => UnitTest_Long_AStrings.To_Long_String ("long_message")));
      UnitTest_Listeners.Basic.End_Test
        (Listener,
        (Phase      => TEST_END, Test_Name => To_Bounded_Wide_Wide_String ("testname"),
          Test_Kind => ROUTINE));

      UnitTest_Listeners.Basic.End_Test
        (Listener,
        (Phase      => TEST_END, Test_Name => To_Bounded_Wide_Wide_String ("suite"),
          Test_Kind => CONTAINER));

      Assert_Eq (Test_Count (Listener.Main_Result), 1, "Test Count");

      Assert_Eq
        (Direct_Test_Count (Listener.Main_Result), 0, "Direct Test Count");

      Assert_Eq (Error_Count (Listener.Main_Result), 1, "Error Count");
   end Test_Error_Inside_Suite;

end Basic_Listener_Tests;
