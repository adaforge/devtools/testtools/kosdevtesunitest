--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: ISC
--  SPDX-Creator: Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2007-2022 Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2023 adaforge MARINE s.a.s. (william.franck@adaforge.io)
--  SPDX-FileContributor: 2023 William J. Franck (william.franck@adaforge.io)
--  --------------------------------------------------------------------------------------
--
--  @summary
--
--  @description
--
--  --------------------------------------------------------------------------------------

with AdaForge.DevTools.TestTools.UnitTest;
use AdaForge.DevTools.TestTools;

package SList_Tests is
   type Test is new UnitTest.Test_Case with null record;

   overriding
   procedure Initialize (T : in out Test);

private
   procedure Test_Append_Elementary;
   procedure Test_Append_Record;
   procedure Test_Clear;
   procedure Test_Clear_Empty;
   procedure Test_First;
   procedure Test_Next;
   procedure Test_Data;
   procedure Test_Length;
   procedure Test_Copy;
   procedure Test_For_Each;
end SList_Tests;
