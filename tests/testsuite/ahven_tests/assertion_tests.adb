--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: ISC
--  SPDX-Creator: Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2007-2022 Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2023 adaforge MARINE s.a.s. (william.franck@adaforge.io)
--  SPDX-FileContributor: 2023 William J. Franck (william.franck@adaforge.io)
--  --------------------------------------------------------------------------------------

with AdaForge.DevTools.TestTools.UnitTest;
use AdaForge.DevTools.TestTools.UnitTest;

package body Assertion_Tests is
   procedure Assert_Int_Equal is new Assert_Equal
     (Data_Type => Integer, Image => Integer'Wide_Wide_Image);

   overriding
   procedure Initialize (T : in out Test) is
   begin
      Set_Name (T, "AUnit");

      UnitTest.Add_Test_Routine (T, Test_Assert_Equal'Access, "Assert_Equal");

      UnitTest.Add_Test_Routine (T, Test_Assert'Access, "Assert");
      UnitTest.Add_Test_Routine (T, Test_Fail'Access, "Fail");
   end Initialize;

   procedure Test_Assert_Equal is
      Exception_Got : Boolean;
   begin
      begin
         Exception_Got := False;
         Assert_Int_Equal
           (Expected => 1, Actual => 1, Message => "Assert_Equal");
      exception
         when Assertion_Error =>
            Exception_Got := True;
      end;
      Assert (not Exception_Got, "exception for valid condition!");

      begin
         Exception_Got := False;
         Assert_Int_Equal
           (Expected => 1, Actual => 2, Message => "Assert_Equal");
      exception
         when Assertion_Error =>
            Exception_Got := True;
      end;
      Assert (Exception_Got, "no exception for invalid condition!");
   end Test_Assert_Equal;

   procedure Test_Fail is
      Exception_Got : Boolean;
   begin
      begin
         Exception_Got := False;
         Fail ("fail");
      exception
         when Assertion_Error =>
            Exception_Got := True;
      end;
      Assert (Exception_Got, "Fail did not raise exception!");
   end Test_Fail;

   procedure Test_Assert is
      Exception_Got : Boolean;
   begin
      Assert (True, "Assert (True)");

      begin
         Exception_Got := False;
         Assert (False, "assertion");
      exception
         when Assertion_Error =>
            Exception_Got := True;
      end;
      if not Exception_Got then
         --  Raising Assertion_Error directly, since Assert apparently
         --  does not work.
         raise Assertion_Error;
      end if;
   end Test_Assert;

end Assertion_Tests;
