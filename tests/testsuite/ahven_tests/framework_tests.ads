--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: ISC
--  SPDX-Creator: Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2007-2022 Tero Koskinen (tero.koskinen@iki.fi)
--  SPDX-FileCopyrightText: Copyright 2023 adaforge MARINE s.a.s. (william.franck@adaforge.io)
--  SPDX-FileContributor: 2023 William J. Franck (william.franck@adaforge.io)
--  --------------------------------------------------------------------------------------
--
--  @summary
--
--  @description
--
--  --------------------------------------------------------------------------------------

with AdaForge.DevTools.TestTools.UnitTest;
use AdaForge.DevTools.TestTools;

package Framework_Tests is
   pragma Elaborate_Body;  -- due to myTest object initialization (?)

   type Test_State is (UNINITIALIZED, INITIALIZED, SETUP_DONE, TEARDOWN_DONE);
   type Test is new UnitTest.Test_Case with record
      Value : Test_State := UNINITIALIZED;
   end record;

   overriding
   procedure Initialize (T : in out Test);

   overriding
   procedure Set_Up (T : in out Test);

   overriding
   procedure Tear_Down (T : in out Test);

private
   procedure Test_Set_Up0; -- helper for Test_Set_Up()
   procedure Test_Set_Up (T : in out Test);

   procedure Test_Set_Up_And_Tear_Down_Simple;

   procedure Test_Tear_Down_After_Exception_Simple;

   procedure Test_Tear_Down;

   procedure Test_Test_Case_Run;

   procedure Test_Test_Case_Run_Empty;

   procedure Test_Test_Case_Run_1s_Timeout;

   procedure Test_Test_Case_Run_Break_Infinite_Loop;

   procedure Test_Test_Case_Test_Count;

   procedure Test_Test_Case_Truncate_Name;

   procedure Test_Test_Suite_Run;

   procedure Test_Test_Suite_Static_Run;

   procedure Test_Test_Suite_Name_Run;

   procedure Test_Call_End_Test;

   procedure Test_Test_Suite_Inside_Suite;

   procedure Test_Test_Suite_Test_Count;

   procedure Test_Test_Suite_Test_Static_Count;

   procedure Test_Test_Suite_Test_Name_Count;

   procedure Test_Test_Suite_Cleanup;

end Framework_Tests;
